package hk.quantr.quantrverilogtool;

import hk.quantr.quantrverilogtool.antlr.Verilog2001BaseListener;
import hk.quantr.quantrverilogtool.antlr.Verilog2001Parser;
import java.util.ArrayList;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class MyVerilogListener extends Verilog2001BaseListener {

	ArrayList<String> inputs = new ArrayList<>();
	ArrayList<String> outputs = new ArrayList<>();
	String moduleName;

	@Override
	public void exitInput_declaration(Verilog2001Parser.Input_declarationContext ctx) {
//		System.out.println("input=" + ctx.list_of_port_identifiers().getText());
		inputs.add(ctx.list_of_port_identifiers().getText());
	}

	@Override
	public void exitOutput_declaration(Verilog2001Parser.Output_declarationContext ctx) {
//		System.out.println("output=" + ctx.list_of_port_identifiers().getText());
		outputs.add(ctx.list_of_port_identifiers().getText());
	}

	@Override
	public void exitModule_declaration(Verilog2001Parser.Module_declarationContext ctx) {
		moduleName = ctx.module_identifier().getText();
	}

}
