package hk.quantr.quantrverilogtool;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import hk.quantr.javalib.PropertyUtil;
import hk.quantr.quantrverilogtool.antlr.Verilog2001Lexer;
import hk.quantr.quantrverilogtool.antlr.Verilog2001Parser;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.atn.PredictionMode;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class QuantrVerilogTool {

	private static final Logger logger = Logger.getLogger(QuantrVerilogTool.class.getName());

	public static void main(String args[]) throws ParseException, FileNotFoundException, IOException {
		Options options = new Options();
		options.addOption("v", "version", false, "display version");
		options.addOption("h", "help", false, "help");
		options.addOption("i", false, "generate json for in/out ports summary");

		if (Arrays.asList(args).contains("-h") || Arrays.asList(args).contains("--help")) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("java -jar quantr-verlog-tool.jar [OPTION] <input file>", options);
			return;
		}
		if (Arrays.asList(args).contains("-v") || Arrays.asList(args).contains("--version")) {
			System.out.println("version : " + PropertyUtil.getProperty("main.properties", "version"));

			TimeZone utc = TimeZone.getTimeZone("UTC");
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			format.setTimeZone(utc);
			Calendar cl = Calendar.getInstance();
			try {
				Date convertedDate = format.parse(PropertyUtil.getProperty("main.properties", "build.date"));
				cl.setTime(convertedDate);
				cl.add(Calendar.HOUR, 8);
			} catch (java.text.ParseException ex) {
				Logger.getLogger(QuantrVerilogTool.class.getName()).log(Level.SEVERE, null, ex);
			}
			System.out.println("build date : " + format.format(cl.getTime()) + " HKT");
			return;
		}
		CommandLineParser cliParser = new DefaultParser();
		CommandLine cmd = cliParser.parse(options, args);
		List<String> arguments = cmd.getArgList();
		if (arguments.isEmpty()) {
			logger.log(Level.SEVERE, "Please specific input file");
			System.exit(2);
		} else {
			if (!new File(arguments.get(0)).exists() || !new File(arguments.get(0)).isFile()) {
				logger.log(Level.SEVERE, arguments.get(0) + " not exist");
				System.exit(4);
			}
			if (cmd.hasOption("i")) {
				Output outputs[] = new Output[arguments.size()];
				for (int x = 0; x < arguments.size(); x++) {
					String content = IOUtils.toString(new FileInputStream(arguments.get(x)), "utf-8");
//					Verilog2001Lexer lexer = new Verilog2001Lexer(CharStreams.fromString(content));
//					CommonTokenStream tokenStream = new CommonTokenStream(lexer);
//					MyVerilogListener listener = new MyVerilogListener();
//					Verilog2001Parser parser = new Verilog2001Parser(tokenStream);
//					parser.addParseListener(listener);
//					Verilog2001Parser.Source_textContext context = parser.source_text();
//
//					Output output = new Output();
//					output.moduleName = listener.moduleName;
//					output.inputs = listener.inputs;
//					output.outputs = listener.outputs;
//					outputs[x] = output;
					outputs[x] = getOutput(content);
				}
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				if (outputs.length == 1) {
					System.out.println(gson.toJson(outputs[0]));
				} else {
					System.out.println(gson.toJson(outputs));
				}
				return;
			}
		}
	}

	public static Output getOutput(String content) {
		Verilog2001Lexer lexer = new Verilog2001Lexer(CharStreams.fromString(content));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		MyVerilogListener listener = new MyVerilogListener();
		Verilog2001Parser parser = new Verilog2001Parser(tokenStream);
		parser.getInterpreter().setPredictionMode(PredictionMode.SLL);
		parser.addParseListener(listener);
		Verilog2001Parser.Source_textContext context = parser.source_text();

		Output output = new Output();
		output.moduleName = listener.moduleName;
		output.inputs = listener.inputs;
		output.outputs = listener.outputs;
		return output;
	}
}
