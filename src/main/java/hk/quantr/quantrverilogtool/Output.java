package hk.quantr.quantrverilogtool;

import java.util.ArrayList;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Output {

	public String moduleName;
	public ArrayList<String> inputs;
	public ArrayList<String> outputs;
}
