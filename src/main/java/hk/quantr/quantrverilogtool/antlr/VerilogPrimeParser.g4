parser grammar VerilogPrimeParser;

@lexer::members {
    private boolean isPragma(){
        StringBuilder commentText = new StringBuilder();
        int ahead = 1;
        int next = _input.LA(ahead++);
        commentText.append((char)next);
        while(next != -1 && next != '\r' && next != '\n')
        {
            next = _input.LA(ahead++);
            commentText.append((char)next);
        }

        if(commentText.toString().trim().startsWith("pragma")) {
            return true;
        }
        else {
            // Let the lexer consume all the characters!
            for(int i = 0; i < commentText.length(); i++)
            {
                _input.consume();
            }
            return false;
        }
    }
}

@header{
}

options{
    tokenVocab=VerilogPrimeLexer;
}

source_text
locals []
:  timeunits_declaration?  description*  eof ;

description
locals []
:  module_declaration 
|  udp_declaration 
|  interface_declaration 
|  program_declaration 
|  checker_declaration 
|  package_declaration 
|  attribute_instance* (  package_item  |  bind_directive  );

module_declaration
locals []
:  module_nonansi_header  timeunits_declaration?  module_item*  endmodulestr (  colon  module_identifier  )? 
|  module_ansi_header  timeunits_declaration?  non_port_module_item*  endmodulestr (  colon  module_identifier  )? 
|  externstr (  module_nonansi_header  |  module_ansi_header  );

module_nonansi_header
locals []
:  attribute_instance*  module_keyword  lifetime?  module_identifier (  package_import_declaration  )*  parameter_port_list?  list_of_ports  semi pragma_def? ;

module_ansi_header
locals []
:  attribute_instance*  module_keyword  lifetime?  module_identifier (  package_import_declaration  )*  parameter_port_list?  list_of_port_declarations?  semi  pragma_def?;

module_keyword
locals []
:  modulestr 
|  macromodulestr ;

interface_declaration
locals []
:  interface_nonansi_header  timeunits_declaration?  interface_item*  endinterfacestr (  colon  interface_identifier  )? 
|  interface_ansi_header  timeunits_declaration?  non_port_interface_item*  endinterfacestr (  colon  interface_identifier  )? 
|  attribute_instance*  interfacestr  interface_identifier  lparen  dotstar  rparen  semi  timeunits_declaration?  interface_item*  endinterfacestr (  colon  interface_identifier  )? 
|  externstr (  interface_nonansi_header  |  interface_ansi_header  );

interface_nonansi_header
locals []
:  attribute_instance*  interfacestr  lifetime?  interface_identifier (  package_import_declaration  )*  parameter_port_list?  list_of_ports  semi ;

interface_ansi_header
locals []
:  attribute_instance*  interfacestr  lifetime?  interface_identifier  package_import_declaration*  parameter_port_list?  list_of_port_declarations? ;

program_declaration
locals []
:  program_nonansi_header  timeunits_declaration?  program_item*  endprogramstr (  colon  program_identifier  )? 
|  program_ansi_header  timeunits_declaration? (  non_port_program_item  )*  endprogramstr (  colon  program_identifier  )? 
|  attribute_instance*  programstr  program_identifier  lparen  dotstar  rparen  semi  timeunits_declaration?  program_item*  endprogramstr (  colon  program_identifier  )? 
|  externstr (  program_nonansi_header  |  program_ansi_header  );

program_nonansi_header
locals []
:  attribute_instance*  programstr  lifetime?  program_identifier (  package_import_declaration  )*  parameter_port_list?  list_of_ports  semi ;

program_ansi_header
locals []
:  attribute_instance*  programstr  lifetime?  program_identifier (  package_import_declaration  )*  parameter_port_list?  list_of_port_declarations?  semi ;

checker_declaration
locals []
:  checkerstr  checker_identifier (  lparen  checker_port_list?  rparen  )?  semi  checker_or_generate_item*  endcheckerstr (  colon  checker_identifier  )? ;

class_declaration
locals []
: (  virtualstr  )?  classstr  lifetime?  class_identifier  parameter_port_list? (  extendsstr  class_type (  lparen  list_of_arguments  rparen  )?  )?  semi  class_item*  endclassstr (  colon  class_identifier  )? ;

package_declaration
locals []
:  attribute_instance*  packagestr  lifetime?  package_identifier  semi  timeunits_declaration? (  package_declaration_part1  )*  endpackagestr (  colon  package_identifier  )? ;

package_declaration_part1
locals []
:  attribute_instance*  package_item ;

timeunits_declaration
locals []
:  timeunit  time_literal (  div  time_literal  )?  semi ;

parameter_port_list
locals []
:  hash  lparen ( (  list_of_param_assignments (  comma  parameter_port_declaration  )*  ) |  list_of_parameter_port_declaration  )?  rparen ;

list_of_parameter_port_declaration
locals []
:  parameter_port_declaration (  comma  parameter_port_declaration  )* ;

parameter_port_declaration
locals []
:  parameter_declaration 
|  local_parameter_declaration 
|  data_type  list_of_param_assignments 
|  typestr  list_of_type_assignments ;

list_of_ports
locals []
:  lparen  port (  comma  port  )*  rparen ;

list_of_port_declarations
locals []
:  lparen (  list_of_port_declarations_part1 (  comma  list_of_port_declarations_part1  )*  )?  rparen ;

list_of_port_declarations_part1
locals []
:  attribute_instance*  ansi_port_declaration ;

port_declaration
locals []
:  attribute_instance* (  inout_declaration  |  input_declaration  |  output_declaration  |  ref_declaration  |  interface_port_declaration  );

port
locals []
: (  port_expression  )? 
|  dot  port_identifier  lparen  port_expression?  rparen ;

port_expression
locals []
:  port_reference 
|  lcurl  port_reference (  comma  port_reference  )*  rcurl ;

port_reference
locals []
:  port_identifier  constant_select ;

port_direction
locals []
:  inputstr 
|  outputstr 
|  inoutstr 
|  refstr ;

net_port_header
locals []
: (  port_direction  )?  net_port_type ;

variable_port_header
locals []
: (  port_direction  )?  variable_port_type ;

interface_port_header
locals []
:  interface_identifier (  dot  modport_identifier  )? 
|  interfacestr (  dot  modport_identifier  )? ;

ansi_port_declaration
locals []
: (  net_port_header  |  interface_port_header  )?  port_identifier (  unpacked_dimension  )* (  assign  constant_expression  )? 
|  variable_port_header?  port_identifier  variable_dimension* (  assign  constant_expression  )? 
| (  port_direction  )?  dot  port_identifier  lparen  expression?  rparen ;

elaboration_system_task
locals []
:  dollarfatalstr (  lparen  finish_number (  comma  list_of_arguments  )?  rparen  )?  semi 
|  dollarerrorstr (  lparen  list_of_arguments?  rparen  )?  semi 
|  dollarwarningstr (  lparen  list_of_arguments?  rparen  )?  semi 
|  dollarinfostr (  lparen  list_of_arguments?  rparen  )?  semi ;

finish_number
locals []
:  number ;

module_common_item
locals []
:  module_or_generate_item_declaration 
|  interface_instantiation 
|  program_instantiation 
|  assertion_item 
|  bind_directive 
|  continuous_assign 
|  net_alias 
|  initial_construct 
|  final_construct 
|  always_construct 
|  loop_generate_construct 
|  conditional_generate_construct 
|  elaboration_system_task ;

module_item
locals []
:  port_declaration  semi 
|  non_port_module_item ;

module_or_generate_item
locals []
:  attribute_instance*  parameter_override 
|  attribute_instance*  gate_instantiation 
|  attribute_instance*  udp_instantiation 
|  attribute_instance*  module_instantiation 
|  attribute_instance*  module_common_item ;

non_port_module_item
locals []
:  generate_region 
|  module_or_generate_item 
|  specify_block 
|  attribute_instance*  specparam_declaration 
|  program_declaration 
|  module_declaration 
|  interface_declaration 
|  timeunits_declaration 
|  checker_declaration ;

parameter_override
locals []
:  defparamstr  list_of_defparam_assignments  semi ;

bind_directive
locals []
:  bindstr  bind_target_scope (  colon  bind_target_instance_list  )?  bind_instantiation  semi 
|  bindstr  bind_target_instance  bind_instantiation  semi ;

bind_target_scope
locals []
:  module_identifier 
|  interface_identifier ;

bind_target_instance
locals []
:  hierarchical_identifier  constant_bit_select ;

bind_target_instance_list
locals []
:  bind_target_instance (  comma  bind_target_instance  )* ;

bind_instantiation
locals []
:  program_instantiation 
|  module_instantiation 
|  interface_instantiation 
|  checker_instantiation ;

config_declaration
locals []
:  configstr  config_identifier  semi (  local_parameter_declaration  )*  design_statement (  config_rule_statement  )*  endconfigstr (  colon  config_identifier  )? ;

design_statement
locals []
:  designstr  design_statement_part1*  semi ;

design_statement_part1
locals []
: (  library_identifier  dot  )?  cell_identifier ;

config_rule_statement
locals []
:  default_clause  liblist_clause  semi 
|  inst_clause  liblist_clause  semi 
|  inst_clause  use_clause  semi 
|  cell_clause  liblist_clause  semi 
|  cell_clause  use_clause  semi ;

default_clause
locals []
:  defaultstr ;

inst_clause
locals []
:  instancestr  inst_name ;

inst_name
locals []
:  topmodule_identifier (  dot  instance_identifier  )* ;

cell_clause
locals []
:  cellstr (  library_identifier  dot  )?  cell_identifier ;

liblist_clause
locals []
:  libliststr (  library_identifier  )? ;

use_clause
locals []
:  usestr (  library_identifier  dot  )?  cell_identifier (  colon  configstr  )? 
|  usestr  named_parameter_assignment (  comma  named_parameter_assignment  )* (  colon  configstr  )? 
|  usestr (  library_identifier  dot  )?  cell_identifier  named_parameter_assignment (  comma  named_parameter_assignment  )* (  colon  configstr  )? ;

module_or_generate_item_declaration
locals []
:  package_or_generate_item_declaration 
|  genvar_declaration 
|  clocking_declaration 
|  defaultstr  clockingstr  clocking_identifier  semi 
|  defaultstr  disablestr  iffstr  expression_or_dist  semi ;

interface_or_generate_item
locals []
:  attribute_instance*  module_common_item 
|  attribute_instance*  modport_declaration 
|  attribute_instance*  extern_tf_declaration ;

extern_tf_declaration
locals []
:  externstr  method_prototype  semi 
|  externstr  forkjoinstr  task_prototype  semi ;

interface_item
locals []
:  port_declaration  semi 
|  non_port_interface_item ;

non_port_interface_item
locals []
:  generate_region 
|  interface_or_generate_item 
|  program_declaration 
|  interface_declaration 
|  checker_declaration 
|  timeunits_declaration ;

program_item
locals []
:  port_declaration  semi 
|  non_port_program_item ;

non_port_program_item
locals []
:  attribute_instance*  continuous_assign 
|  attribute_instance*  module_or_generate_item_declaration 
|  attribute_instance*  initial_construct 
|  attribute_instance*  final_construct 
|  attribute_instance*  concurrent_assertion_item 
|  attribute_instance*  timeunits_declaration 
|  program_generate_item ;

program_generate_item
locals []
:  loop_generate_construct 
|  conditional_generate_construct 
|  generate_region 
|  elaboration_system_task ;

checker_port_list
locals []
:  checker_port_item (  comma  checker_port_item  )* ;

checker_port_item
locals []
:  attribute_instance*  property_formal_type  port_identifier  variable_dimension* (  assign  property_actual_arg  )? ;

checker_or_generate_item
locals []
:  checker_or_generate_item_declaration 
|  initial_construct 
|  checker_always_construct 
|  final_construct 
|  assertion_item 
|  checker_generate_item ;

checker_or_generate_item_declaration
locals []
:  data_declaration 
|  function_declaration 
|  assertion_item_declaration 
|  covergroup_declaration 
|  overload_declaration 
|  genvar_declaration 
|  clocking_declaration 
|  defaultstr  clockingstr  clocking_identifier  semi 
|  defaultstr  disablestr  iffstr  expression_or_dist  semi 
|  semi ;

checker_generate_item
locals []
:  loop_generate_construct 
|  conditional_generate_construct 
|  generate_region 
|  elaboration_system_task ;

checker_always_construct
locals []
:  alwaysstr  statement ;

class_item
locals []
:  attribute_instance*  class_property 
|  attribute_instance*  class_method 
|  attribute_instance*  class_constraint 
|  attribute_instance*  class_declaration 
|  attribute_instance*  covergroup_declaration 
|  local_parameter_declaration  semi 
|  parameter_declaration  semi 
|  semi ;

class_property
locals []
: (  property_qualifier  )*  data_declaration 
|  conststr  class_item_qualifier*  data_type  const_identifier (  assign  constant_expression  )?  semi ;

class_method
locals []
:  method_qualifier*  task_declaration 
|  method_qualifier*  function_declaration 
|  externstr  method_qualifier*  method_prototype  semi 
|  method_qualifier*  class_constructor_declaration 
|  externstr  method_qualifier*  class_constructor_prototype ;

class_constructor_prototype
locals []
:  functionstr  newstr  lparen  tf_port_list?  rparen  semi ;

class_constraint
locals []
:  constraint_prototype 
|  constraint_declaration ;

class_item_qualifier
locals []
:  staticstr 
|  protectedstr 
|  localstr ;

property_qualifier
locals []
:  random_qualifier 
|  class_item_qualifier ;

random_qualifier
locals []
:  randstr 
|  randcstr ;

method_qualifier
locals []
:  purestr?  virtualstr 
|  class_item_qualifier ;

method_prototype
locals []
:  task_prototype 
|  function_prototype ;

class_constructor_declaration
locals []
:  functionstr  class_scope?  newstr (  lparen  tf_port_list?  rparen  )?  semi  block_item_declaration? (  superstr  dot  newstr (  lparen  list_of_arguments  rparen  )?  semi  )?  function_statement_or_null*  endfunctionstr (  colon  newstr  )? ;

constraint_declaration
locals []
: (  staticstr  )?  constraintstr  constraint_identifier  constraint_block ;

constraint_block
locals []
:  lcurl  constraint_block_item*  rcurl ;

constraint_block_item
locals []
:  solvestr  solve_before_list  beforestr  solve_before_list  semi 
|  constraint_expression ;

solve_before_list
locals []
:  solve_before_primary (  comma  solve_before_primary  )* ;

solve_before_primary
locals []
: (  implicit_class_handle  dot  |  class_scope  )?  hierarchical_identifier  select ;

constraint_expression
locals []
:  expression_or_dist  semi 
|  expression  derive  constraint_set 
|  ifstr  lparen  expression  rparen  constraint_set (  elsestr  constraint_set  )? 
|  foreachstr  lparen  ps_or_hierarchical_array_identifier  lbrack  loop_variables  rbrack  rparen  constraint_set ;

constraint_set
locals []
:  constraint_expression 
|  lcurl (  constraint_expression  )*  rcurl ;

dist_list
locals []
:  dist_item (  comma  dist_item  )* ;

dist_item
locals []
:  value_range  dist_weight? ;

dist_weight
locals []
:  colonequals  expression 
|  colonslash  expression ;

constraint_prototype
locals []
:  staticstr?  constraintstr  constraint_identifier  semi ;

extern_constraint_declaration
locals []
:  staticstr?  constraintstr  class_scope  constraint_identifier  constraint_block ;

identifier_list
locals []
:  identifier (  comma  identifier  )* ;

package_item
locals []
:  package_or_generate_item_declaration 
|  anonymous_program 
|  package_export_declaration 
|  timeunits_declaration ;

package_or_generate_item_declaration
locals []
:  net_declaration 
|  data_declaration 
|  task_declaration 
|  function_declaration 
|  dpi_import_export 
|  extern_constraint_declaration 
|  class_declaration 
|  class_constructor_declaration 
|  local_parameter_declaration  semi 
|  parameter_declaration  semi 
|  covergroup_declaration 
|  overload_declaration 
|  assertion_item_declaration 
|  semi ;

anonymous_program
locals []
:  programstr  semi  anonymous_program_item*  endprogramstr ;

anonymous_program_item
locals []
:  task_declaration 
|  function_declaration 
|  class_declaration 
|  covergroup_declaration 
|  class_constructor_declaration 
|  semi ;

local_parameter_declaration
locals []
:  localparamstr  data_type_or_implicit  list_of_param_assignments 
|  localparamstr  typestr  list_of_type_assignments ;

parameter_declaration
locals []
:  parameterstr  data_type_or_implicit  list_of_param_assignments 
|  parameterstr  typestr  list_of_type_assignments ;

specparam_declaration
locals []
:  specparamstr  packed_dimension?  list_of_specparam_assignments  semi ;

inout_declaration
locals []
:  inoutstr  net_port_type  list_of_port_identifiers ;

input_declaration
locals []
:  inputstr  net_port_type  list_of_port_identifiers 
|  inputstr  variable_port_type  list_of_variable_identifiers ;

output_declaration
locals []
:  outputstr  net_port_type  list_of_port_identifiers 
|  outputstr  variable_port_type  list_of_variable_port_identifiers ;

interface_port_declaration
locals []
:  interface_identifier  list_of_interface_identifiers 
|  interface_identifier  dot  modport_identifier  list_of_interface_identifiers ;

ref_declaration
locals []
:  refstr  variable_port_type  list_of_port_identifiers ;

data_declaration
locals []
: (  conststr  )? (  varstr  )? (  lifetime  )?  data_type_or_implicit  list_of_variable_decl_assignments  semi 
|  type_declaration 
|  package_import_declaration 
|  virtual_interface_declaration ;

package_import_declaration
locals []
:  importstr  package_import_item (  comma  package_import_item  )*  semi ;

package_import_item
locals []
:  package_identifier  coloncolon  identifier 
|  package_identifier  coloncolon  star ;

package_export_declaration
locals []
:  export  startcoloncolonstar  semi 
|  export  package_import_item (  comma  package_import_item  )*  semi ;

genvar_declaration
locals []
:  genvarstr  list_of_genvar_identifiers  semi ;

net_declaration
locals []
:  net_type (  drive_strength  |  charge_strength  )? (  vectoredstr  |  scalaredstr  )?  data_type_or_implicit  delay3?  list_of_net_decl_assignments  semi ;

type_declaration
locals []
:  typedefstr  data_type  type_identifier (  variable_dimension  )*  semi 
|  typedefstr  interface_instance_identifier  constant_bit_select  dot  type_identifier  type_identifier  semi 
|  typedefstr (  enumstr  |  structstr  |  unionstr  |  classstr  )?  type_identifier  semi ;

lifetime
locals []
:  staticstr 
|  automaticstr ;

casting_type
locals []
:  simple_type 
|  signing 
|  stringstr 
|  conststr ;

data_type
locals []
:  integer_vector_type  signing? (  packed_dimension  )* 
|  integer_atom_type  signing? 
|  non_integer_type 
|  struct_union (  packedstr (  signing  )?  )?  lcurl  struct_union_member (  struct_union_member  )*  rcurl  packed_dimension* 
|  enumstr  enum_base_type?  lcurl  enum_name_declaration (  comma  enum_name_declaration  )*  rcurl  packed_dimension* 
|  stringstr 
|  chandlestr 
|  virtualstr (  interfacestr  )?  interface_identifier 
| (  class_scope  |  package_scope  )?  type_identifier  packed_dimension* 
|  class_type 
|  eventstr 
|  ps_covergroup_identifier 
|  type_reference ;

data_type_or_implicit
locals []
:  data_type 
|  implicit_data_type ;

implicit_data_type
locals []
: (  signing  )?  packed_dimension* ;

enum_base_type
locals []
:  integer_atom_type (  signing  )? 
|  integer_vector_type  signing? (  packed_dimension  )? 
|  type_identifier (  packed_dimension  )? ;

enum_name_declaration
locals []
:  enum_identifier (  enum_name_declaration_part1 (  colon (  zero_or_one  |  edge_spec  |  decimal_number  ) )?  rbrack  )? (  assign  constant_expression  )? ;

enum_name_declaration_part1
locals []
:  lbrack (  zero_or_one  |  edge_spec  |  decimal_number  );

class_scope
locals []
:  class_type  coloncolon ;

class_type
locals []
:  ps_class_identifier (  parameter_value_assignment  )?  class_type_part1* ;

class_type_part1
locals []
:  coloncolon  class_identifier (  parameter_value_assignment  )? ;

integer_type
locals []
:  integer_vector_type 
|  integer_atom_type ;

integer_atom_type
locals []
:  bytestr 
|  shortintstr 
|  intstr 
|  longintstr 
|  integerstr 
|  timestr ;

integer_vector_type
locals []
:  bitstr 
|  logicstr 
|  regstr ;

non_integer_type
locals []
:  shortreal 
|  realstr 
|  realtimestr ;

net_type
locals []
:  supply0str 
|  supply1str 
|  tristr 
|  triandstr 
|  triorstr 
|  triregstr 
|  tri0str 
|  tri1str 
|  uwirestr 
|  wirestr 
|  wandstr 
|  worstr ;

net_port_type
locals []
:  net_type?  data_type_or_implicit ;

variable_port_type
locals []
:  var_data_type ;

var_data_type
locals []
:  data_type 
|  varstr  data_type_or_implicit ;

signing
locals []
:  signedstr 
|  unsignedstr ;

simple_type
locals []
:  integer_type 
|  non_integer_type 
|  ps_type_identifier 
|  ps_parameter_identifier ;

struct_union_member
locals []
:  attribute_instance*  random_qualifier?  data_type_or_void  list_of_variable_decl_assignments  semi ;

data_type_or_void
locals []
:  data_type 
|  voidstr ;

struct_union
locals []
:  structstr 
|  unionstr (  taggedstr  )? ;

type_reference
locals []
:  typestr  lparen  expression  rparen 
|  typestr  lparen  data_type  rparen ;

drive_strength
locals []
:  lparen  strength0  comma  strength1  rparen 
|  lparen  strength1  comma  strength0  rparen 
|  lparen  strength0  comma  highz1str  rparen 
|  lparen  strength1  comma  highz0str  rparen 
|  lparen  highz0str  comma  strength1  rparen 
|  lparen  highz1str  comma  strength0  rparen ;

strength0
locals []
:  supply0str 
|  strong0 
|  pull0str 
|  weak0str ;

strength1
locals []
:  supply1str 
|  strong1 
|  pull1str 
|  weak1str ;

charge_strength
locals []
:  lparen  smallstr  rparen 
|  lparen  mediumstr  rparen 
|  lparen  largestr  rparen ;

delay3
locals []
:  hash  delay_value 
|  hash  lparen  mintypmax_expression (  comma  mintypmax_expression (  comma  mintypmax_expression  )?  )?  rparen ;

delay2
locals []
:  hash  delay_value 
|  hash  lparen  mintypmax_expression (  comma  mintypmax_expression  )?  rparen ;

delay_value
locals []
:  edge_spec 
|  zero_or_one 
|  decimal_number 
|  real_number 
|  ps_identifier 
|  time_literal 
|  onestepstr ;

list_of_defparam_assignments
locals []
:  defparam_assignment (  comma  defparam_assignment  )* ;

list_of_genvar_identifiers
locals []
:  genvar_identifier (  comma  genvar_identifier  )* ;

list_of_interface_identifiers
locals []
:  list_of_interface_identifiers_part1 (  comma  list_of_interface_identifiers_part1  )* ;

list_of_interface_identifiers_part1
locals []
:  interface_identifier  unpacked_dimension* ;

list_of_param_assignments
locals []
:  param_assignment (  comma  param_assignment  )* ;

list_of_port_identifiers
locals []
:  list_of_port_identifiers_part1 (  comma  list_of_port_identifiers_part1  )* ;

list_of_port_identifiers_part1
locals []
:  port_identifier  unpacked_dimension* ;

list_of_udp_port_identifiers
locals []
:  port_identifier (  comma  port_identifier  )* ;

list_of_specparam_assignments
locals []
:  specparam_assignment (  comma  specparam_assignment  )* ;

list_of_tf_variable_identifiers
locals []
:  list_of_tf_variable_identifiers_part1 (  comma  list_of_tf_variable_identifiers_part1  )* ;

list_of_tf_variable_identifiers_part1
locals []
:  port_identifier (  variable_dimension  )* (  assign  expression  )? ;

list_of_type_assignments
locals []
:  type_assignment (  comma  type_assignment  )* ;

list_of_variable_decl_assignments
locals []
:  variable_decl_assignment (  comma  variable_decl_assignment  )* ;

list_of_variable_identifiers
locals []
:  list_of_variable_identifiers_part1 (  comma  list_of_variable_identifiers_part1  )* ;

list_of_variable_identifiers_part1
locals []
:  variable_identifier  variable_dimension* ;

list_of_variable_port_identifiers
locals []
:  list_of_variable_port_identifiers_part1 (  comma  list_of_variable_port_identifiers_part1  )* ;

list_of_variable_port_identifiers_part1
locals []
:  port_identifier  variable_dimension* (  assign  constant_expression  )? ;

list_of_virtual_interface_decl
locals []
:  list_of_virtual_interface_decl_part1 (  comma  list_of_virtual_interface_decl_part1  )* ;

list_of_virtual_interface_decl_part1
locals []
:  variable_identifier (  assign  interface_instance_identifier  )? ;

defparam_assignment
locals []
:  hierarchical_parameter_identifier  assign  constant_mintypmax_expression ;

list_of_net_decl_assignments
locals []
:  net_decl_assignment (  comma  net_decl_assignment  )* ;

net_decl_assignment
locals []
:  net_identifier  unpacked_dimension* (  assign  expression  )? ;

param_assignment
locals []
:  parameter_identifier (  unpacked_dimension  )* (  assign  constant_param_expression  )* ;

specparam_assignment
locals []
:  specparam_identifier  assign  constant_mintypmax_expression 
|  pulse_control_specparam ;

type_assignment
locals []
:  type_identifier (  assign  data_type  )? ;

pulse_control_specparam
locals []
:  pathpulsedollar  assign  lparen  reject_limit_value (  comma  error_limit_value  )?  rparen 
|  pathpulsedollar  specify_input_terminal_descriptor  dollar  specify_output_terminal_descriptor  assign  lparen  reject_limit_value (  comma  error_limit_value  )?  rparen ;

error_limit_value
locals []
:  limit_value ;

reject_limit_value
locals []
:  limit_value ;

limit_value
locals []
:  constant_mintypmax_expression ;

variable_decl_assignment
locals []
:  variable_identifier  variable_dimension* (  assign  expression  )? 
|  dynamic_array_variable_identifier  unsized_dimension (  variable_dimension  )* (  assign  dynamic_array_new  )? 
|  class_variable_identifier (  assign  class_new  )? ;

class_new
locals []
:  newstr (  lparen  list_of_arguments  rparen  |  expression  )? ;

dynamic_array_new
locals []
:  newstr  lbrack  expression  rbrack (  lparen  expression  rparen  )? ;

unpacked_dimension
locals []
:  lbrack  constant_range  rbrack 
|  lbrack  constant_expression  rbrack ;

packed_dimension
locals []
:  lbrack  constant_range  rbrack 
|  unsized_dimension ;

associative_dimension
locals []
:  lbrack  data_type  rbrack 
|  lbrack  star  rbrack ;

variable_dimension
locals []
:  unsized_dimension 
|  unpacked_dimension 
|  associative_dimension 
|  queue_dimension ;

queue_dimension
locals []
:  lbrack  dollar (  colon  constant_expression  )?  rbrack ;

unsized_dimension
locals []
:  lbrack  rbrack ;

function_data_type_or_implicit
locals []
:  data_type_or_void 
|  implicit_data_type ;

function_declaration
locals []
:  functionstr  lifetime?  function_body_declaration ;

function_body_declaration
locals []
:  function_data_type_or_implicit (  interface_identifier  dot  |  class_scope  )?  function_identifier  semi (  tf_item_declaration  )* (  function_statement_or_null  )*  endfunctionstr (  colon  function_identifier  )? 
|  function_data_type_or_implicit (  interface_identifier  dot  |  class_scope  )?  function_identifier  lparen  tf_port_list?  rparen  semi  block_item_declaration*  function_statement_or_null*  endfunctionstr (  colon  function_identifier  )? ;

function_prototype
locals []
:  functionstr  data_type_or_void  function_identifier  lparen  tf_port_list?  rparen ;

dpi_import_export
locals []
:  importstr  dpi_spec_string  dpi_function_import_property? (  c_identifier  assign  )?  dpi_function_proto  semi 
|  importstr  dpi_spec_string  dpi_task_import_property? (  c_identifier  assign  )?  dpi_task_proto  semi 
|  export  dpi_spec_string (  c_identifier  assign  )?  functionstr  function_identifier  semi 
|  export  dpi_spec_string (  c_identifier  assign  )?  taskstr  task_identifier  semi ;

dpi_spec_string
locals []
:  dpi_spec_ing2str 
|  dpi_spec_ing1str ;

dpi_function_import_property
locals []
:  contextstr 
|  purestr ;

dpi_task_import_property
locals []
:  contextstr ;

dpi_function_proto
locals []
:  function_prototype ;

dpi_task_proto
locals []
:  task_prototype ;

task_declaration
locals []
:  taskstr  lifetime?  task_body_declaration ;

task_body_declaration
locals []
: (  interface_identifier  dot  |  class_scope  )?  task_identifier  semi (  tf_item_declaration  )* (  statement_or_null  )*  endtaskstr (  colon  task_identifier  )? 
| (  interface_identifier  dot  |  class_scope  )?  task_identifier  lparen  tf_port_list?  rparen  semi (  block_item_declaration  )*  statement_or_null*  endtaskstr (  colon  task_identifier  )? ;

tf_item_declaration
locals []
:  block_item_declaration 
|  tf_port_declaration ;

tf_port_list
locals []
:  tf_port_item (  comma  tf_port_item  )* ;

tf_port_item
locals []
:  attribute_instance* (  tf_port_direction  )? (  varstr  )?  data_type_or_implicit (  port_identifier  variable_dimension* (  assign  expression  )?  )? ;

tf_port_direction
locals []
:  port_direction 
|  conststr  refstr ;

tf_port_declaration
locals []
:  attribute_instance*  tf_port_direction (  varstr  )?  data_type_or_implicit  list_of_tf_variable_identifiers  semi ;

task_prototype
locals []
:  taskstr  task_identifier  lparen  tf_port_list?  rparen ;

block_item_declaration
locals []
:  attribute_instance*  data_declaration 
|  attribute_instance*  local_parameter_declaration  semi 
|  attribute_instance*  parameter_declaration  semi 
|  attribute_instance*  overload_declaration 
|  attribute_instance*  let_declaration ;

overload_declaration
locals []
:  bindstr  overload_operator  functionstr  data_type  function_identifier  lparen  overload_proto_formals  rparen  semi ;

overload_operator
locals []
:  plus 
|  increment 
|  minus 
|  decrement 
|  star 
|  starstar 
|  div 
|  modulo 
|  equals 
|  not_equals 
|  lt 
|  le 
|  gt 
|  ge 
|  assign ;

overload_proto_formals
locals []
:  data_type (  comma  data_type  )* ;

virtual_interface_declaration
locals []
:  virtualstr (  interfacestr  )?  interface_identifier (  parameter_value_assignment  )? (  dot  modport_identifier  )?  list_of_virtual_interface_decl  semi ;

modport_declaration
locals []
:  modportstr  modport_item (  comma  modport_item  )*  semi ;

modport_item
locals []
:  modport_identifier  lparen  modport_ports_declaration (  comma  modport_ports_declaration  )*  rparen ;

modport_ports_declaration
locals []
:  attribute_instance*  modport_simple_ports_declaration 
|  attribute_instance*  modport_tf_ports_declaration 
|  attribute_instance*  modport_clocking_declaration ;

modport_clocking_declaration
locals []
:  clockingstr  clocking_identifier ;

modport_simple_ports_declaration
locals []
:  port_direction  modport_simple_port (  comma  modport_simple_port  )* ;

modport_simple_port
locals []
:  port_identifier 
|  dot  port_identifier  lparen  expression?  rparen ;

modport_tf_ports_declaration
locals []
:  import_export  modport_tf_port (  comma  modport_tf_port  )* ;

modport_tf_port
locals []
:  method_prototype 
|  tf_identifier ;

import_export
locals []
:  importstr 
|  export ;

concurrent_assertion_item
locals []
: (  block_identifier  colon  )?  concurrent_assertion_statement 
|  checker_instantiation ;

concurrent_assertion_statement
locals []
:  assert_property_statement 
|  assume_property_statement 
|  cover_property_statement 
|  cover_sequence_statement 
|  restrict_property_statement ;

assert_property_statement
locals []
:  assertstr  propertystr  lparen  property_spec  rparen  action_block ;

assume_property_statement
locals []
:  assumestr  propertystr  lparen  property_spec  rparen  action_block ;

cover_property_statement
locals []
:  coverstr  propertystr  lparen  property_spec  rparen  statement_or_null ;

expect_property_statement
locals []
:  expectstr  lparen  property_spec  rparen  action_block ;

cover_sequence_statement
locals []
:  coverstr  sequencestr  lparen  clocking_event? (  disablestr  iffstr  lparen  expression_or_dist  rparen  )?  sequence_expr  rparen  statement_or_null ;

restrict_property_statement
locals []
:  restrictstr  propertystr  lparen  property_spec  rparen  semi ;

property_instance
locals []
:  ps_or_hierarchical_property_identifier (  lparen  property_list_of_arguments?  rparen  )? ;

property_list_of_arguments
locals []
:  property_actual_arg?  property_list_of_arguments_part1* (  comma  property_list_of_arguments_part2  )* 
|  property_list_of_arguments_part2 (  comma  property_list_of_arguments_part2  )* ;

property_list_of_arguments_part1
locals []
:  comma  property_actual_arg? ;

property_list_of_arguments_part2
locals []
:  dot  identifier  lparen  property_actual_arg?  rparen ;

property_actual_arg
locals []
:  property_expr 
|  sequence_actual_arg ;

assertion_item_declaration
locals []
:  property_declaration 
|  sequence_declaration 
|  let_declaration ;

property_declaration
locals []
:  propertystr  property_identifier (  lparen  property_port_list?  rparen  )?  semi  assertion_variable_declaration*  property_statement_spec  endpropertystr (  colon  property_identifier  )? ;

property_port_list
locals []
:  property_port_item (  comma  property_port_item  )* ;

property_port_item
locals []
:  attribute_instance* (  localstr  property_lvar_port_direction?  )?  property_formal_type  port_identifier  variable_dimension* (  assign  property_actual_arg  )* ;

property_lvar_port_direction
locals []
:  inputstr ;

property_formal_type
locals []
:  sequence_formal_type 
|  propertystr ;

property_spec
locals []
: (  clocking_event  )? (  disablestr  iffstr  lparen  expression_or_dist  rparen  )?  property_expr ;

property_statement_spec
locals []
:  clocking_event? (  disablestr  iffstr  lparen  expression_or_dist  rparen  )?  property_statement ;

property_statement
locals []
:  property_expr  semi 
|  casestr  lparen  expression_or_dist  rparen  property_case_item  property_case_item*  endcasestr 
|  ifstr  lparen  expression_or_dist  rparen  property_expr (  elsestr  property_expr  )? ;

property_case_item
locals []
:  expression_or_dist (  comma  expression_or_dist  )*  colon  property_statement 
|  defaultstr (  colon  )?  property_statement ;

property_expr
locals []
:  sequence_expr 
|  lparen  property_expr  rparen 
|  notstr  property_expr 
|  property_expr  orstr  property_expr 
|  property_expr  andstr  property_expr 
|  sequence_expr  orderive  property_expr 
|  sequence_expr  orimplies  property_expr 
|  ifstr  lparen  expression_or_dist  rparen  property_expr (  elsestr  property_expr  )? 
|  property_instance 
|  clocking_event  property_expr ;

sequence_declaration
locals []
:  sequencestr  sequence_identifier (  lparen  sequence_port_list?  rparen  )?  semi  assertion_variable_declaration*  sequence_expr  semi  endsequencestr (  colon  sequence_identifier  )? ;

sequence_port_list
locals []
:  sequence_port_item (  comma  sequence_port_item  )* ;

sequence_port_item
locals []
:  attribute_instance* (  localstr  sequence_lvar_port_direction?  )?  sequence_formal_type  port_identifier  variable_dimension* (  assign  sequence_actual_arg  )? ;

sequence_lvar_port_direction
locals []
:  inputstr 
|  inoutstr 
|  outputstr ;

sequence_formal_type
locals []
:  data_type_or_implicit 
|  sequencestr 
|  eventstr 
|  untypedstr ;

sequence_expr
locals []
:  cycle_delay_range  sequence_expr (  cycle_delay_range  sequence_expr  )* 
|  sequence_expr  cycle_delay_range  sequence_expr (  cycle_delay_range  sequence_expr  )* 
|  expression_or_dist (  boolean_abbrev  )? 
|  lparen  expression_or_dist (  comma  sequence_match_item  )*  rparen (  boolean_abbrev  )? 
|  sequence_instance (  sequence_abbrev  )? 
|  lparen  sequence_expr (  comma  sequence_match_item  )*  rparen (  sequence_abbrev  )? 
|  sequence_expr  andstr  sequence_expr 
|  sequence_expr  intersectstr  sequence_expr 
|  sequence_expr  orstr  sequence_expr 
|  first_matchstr  lparen  sequence_expr (  comma  sequence_match_item  )*  rparen 
|  expression_or_dist  throughoutstr  sequence_expr 
|  sequence_expr  withinstr  sequence_expr 
|  clocking_event  sequence_expr ;

cycle_delay_range
locals []
:  double_hash  constant_primary 
|  double_hash  lbrack  cycle_delay_const_range_expression  rbrack 
|  double_hash  lbrack  star  rbrack 
|  double_hash  lbrack  plus  rbrack ;

sequence_method_call
locals []
:  sequence_instance  dot  method_identifier ;

sequence_match_item
locals []
:  operator_assignment 
|  inc_or_dec_expression 
|  subroutine_call ;

sequence_instance
locals []
:  ps_or_hierarchical_sequence_identifier (  lparen  sequence_list_of_arguments?  rparen  )? ;

sequence_list_of_arguments
locals []
:  sequence_actual_arg?  sequence_list_of_arguments_part1* (  comma  sequence_list_of_arguments_part2  )* 
|  sequence_list_of_arguments_part2 (  comma  sequence_list_of_arguments_part2  )* ;

sequence_list_of_arguments_part1
locals []
:  comma  sequence_actual_arg? ;

sequence_list_of_arguments_part2
locals []
:  dot  identifier  lparen  sequence_actual_arg?  rparen ;

sequence_actual_arg
locals []
:  event_expression 
|  sequence_expr ;

boolean_abbrev
locals []
:  consecutive_repetition 
|  non_consecutive_repetition 
|  goto_repetition ;

sequence_abbrev
locals []
:  consecutive_repetition ;

consecutive_repetition
locals []
:  lbrack  star  const_or_range_expression  rbrack 
|  lbrack  star  rbrack 
|  lbrack  plus  rbrack ;

non_consecutive_repetition
locals []
:  lbrack  assign  const_or_range_expression  rbrack ;

goto_repetition
locals []
:  lbrack  derive  const_or_range_expression  rbrack ;

const_or_range_expression
locals []
:  constant_expression 
|  cycle_delay_const_range_expression ;

cycle_delay_const_range_expression
locals []
:  constant_expression  colon  constant_expression 
|  constant_expression  colon  dollar ;

expression_or_dist
locals []
:  expression (  diststr  lcurl  dist_list  rcurl  )? ;

assertion_variable_declaration
locals []
:  var_data_type  list_of_variable_decl_assignments  semi ;

let_declaration
locals []
:  letstr  let_identifier (  lparen  let_port_list?  rparen  )?  assign  expression  semi ;

let_identifier
locals []
:  identifier ;

let_port_list
locals []
:  let_port_item (  comma  let_port_item  )* ;

let_port_item
locals []
:  attribute_instance*  let_formal_type  port_identifier  variable_dimension* (  assign  expression  )? ;

let_formal_type
locals []
:  data_type_or_implicit ;

let_expression
locals []
:  package_scope?  let_identifier (  lparen  let_list_of_arguments?  rparen  )? ;

let_list_of_arguments
locals []
:  let_actual_arg?  let_list_of_arguments_part1* (  comma  let_list_of_arguments_part2  )* 
|  let_list_of_arguments_part2 (  comma  let_list_of_arguments_part2  )* ;

let_list_of_arguments_part1
locals []
:  comma  let_actual_arg? ;

let_list_of_arguments_part2
locals []
:  dot  identifier  lparen  let_actual_arg?  rparen ;

let_actual_arg
locals []
:  expression ;

covergroup_declaration
locals []
:  covergroupstr  covergroup_identifier (  lparen (  tf_port_list  )?  rparen  )? (  coverage_event  )?  semi (  coverage_spec_or_option  )*  endgroupstr (  colon  covergroup_identifier  )? ;

coverage_spec_or_option
locals []
: (  attribute_instance  )*  coverage_spec 
| (  attribute_instance  )*  coverage_option  semi ;

coverage_option
locals []
:  optiondot  member_identifier  assign  expression 
|  type_optiondot  member_identifier  assign  constant_expression ;

coverage_spec
locals []
:  cover_point 
|  cover_cross ;

coverage_event
locals []
:  clocking_event 
|  withstr  functionstr  samplestr  lparen (  tf_port_list  )?  rparen 
|  attheratelparen  block_event_expression  rparen ;

block_event_expression
locals []
: (  beginstr  hierarchical_btf_identifier  |  endstr  hierarchical_btf_identifier  )(  orstr  block_event_expression  )* ;

hierarchical_btf_identifier
locals []
:  hierarchical_tf_identifier 
|  hierarchical_block_identifier 
|  hierarchical_identifier (  class_scope  )?  method_identifier ;

cover_point
locals []
: (  cover_point_identifier  colon  )?  coverpointstr  expression (  iffstr  lparen  expression  rparen  )?  bins_or_empty ;

bins_or_empty
locals []
:  lcurl (  attribute_instance  )* (  bins_or_options  semi  )*  rcurl 
|  semi ;

bins_or_options
locals []
:  coverage_option 
| (  wildcardstr  )?  bins_keyword  bins_or_options_part1  assign  lcurl  open_range_list  rcurl (  iffstr  lparen  expression  rparen  )? 
| (  wildcardstr  )?  bins_keyword  bin_identifier (  lbrack  rbrack  )?  assign  trans_list (  iffstr  lparen  expression  rparen  )? 
|  bins_keyword  bins_or_options_part1  assign  defaultstr (  iffstr  lparen  expression  rparen  )? 
|  bins_keyword  bin_identifier  assign  defaultstr  sequencestr (  iffstr  lparen  expression  rparen  )? ;

bins_or_options_part1
locals []
:  bin_identifier (  lbrack (  expression  )?  rbrack  )? ;

bins_keyword
locals []
:  binsstr 
|  illegal_binsstr 
|  ignore_binsstr ;

range_list
locals []
:  value_range (  comma  value_range  )* ;

trans_list
locals []
:  lparen  trans_set  rparen (  comma  lparen  trans_set  rparen  )* ;

trans_set
locals []
:  trans_range_list (  implies  trans_range_list  )* ;

trans_range_list
locals []
:  trans_item 
|  trans_item  lbrack  star  repeat_range  rbrack 
|  trans_item  lbrack  derive  repeat_range  rbrack 
|  trans_item  lbrack  assign  repeat_range  rbrack ;

trans_item
locals []
:  range_list ;

repeat_range
locals []
:  expression 
|  expression  colon  expression ;

cover_cross
locals []
: (  cross_identifier  colon  )?  crossstr  list_of_coverpoints (  iffstr  lparen  expression  rparen  )?  select_bins_or_empty ;

list_of_coverpoints
locals []
:  cross_item  comma  cross_item (  comma  cross_item  )* ;

cross_item
locals []
:  cover_point_identifier 
|  variable_identifier ;

select_bins_or_empty
locals []
:  lcurl (  bins_selection_or_option  semi  )*  rcurl 
|  semi ;

bins_selection_or_option
locals []
:  attribute_instance*  coverage_option 
|  attribute_instance*  bins_selection ;

bins_selection
locals []
:  bins_keyword  bin_identifier  assign  select_expression (  iffstr  lparen  expression  rparen  )? ;

select_expression
locals []
: (  select_condition  |  not  select_condition  |  lparen  select_expression  rparen  ) select_expression_part1* ;

select_expression_part1
locals []
:  log_and  select_expression 
|  log_or  select_expression ;

select_condition
locals []
:  binsofstr  lparen  bins_expression  rparen (  intersectstr  lcurl  open_range_list  rcurl  )? ;

bins_expression
locals []
:  variable_identifier 
|  cover_point_identifier (  dot  bins_identifier  )? ;

open_range_list
locals []
:  open_value_range (  comma  open_value_range  )* ;

open_value_range
locals []
:  value_range ;

gate_instantiation
locals []
:  cmos_switchtype (  delay3  )?  cmos_switch_instance (  comma  cmos_switch_instance  )*  semi 
|  enable_gatetype (  drive_strength  )? (  delay3  )?  enable_gate_instance (  comma  enable_gate_instance  )*  semi 
|  mos_switchtype (  delay3  )?  mos_switch_instance (  comma  mos_switch_instance  )*  semi 
|  n_input_gatetype (  drive_strength  )? (  delay2  )?  n_input_gate_instance (  comma  n_input_gate_instance  )*  semi 
|  n_output_gatetype (  drive_strength  )? (  delay2  )?  n_output_gate_instance (  comma  n_output_gate_instance  )*  semi 
|  pass_en_switchtype (  delay2  )?  pass_enable_switch_instance (  comma  pass_enable_switch_instance  )*  semi 
|  pass_switchtype  pass_switch_instance (  comma  pass_switch_instance  )*  semi 
|  pulldownstr (  pulldown_strength  )?  pull_gate_instance (  comma  pull_gate_instance  )*  semi 
|  pullupstr (  pullup_strength  )?  pull_gate_instance (  comma  pull_gate_instance  )*  semi ;

cmos_switch_instance
locals []
: (  name_of_instance  )?  lparen  output_terminal  comma  input_terminal  comma  ncontrol_terminal  comma  pcontrol_terminal  rparen ;

enable_gate_instance
locals []
: (  name_of_instance  )?  lparen  output_terminal  comma  input_terminal  comma  enable_terminal  rparen ;

mos_switch_instance
locals []
: (  name_of_instance  )?  lparen  output_terminal  comma  input_terminal  comma  enable_terminal  rparen ;

n_input_gate_instance
locals []
: (  name_of_instance  )?  lparen  output_terminal  comma  input_terminal (  comma  input_terminal  )*  rparen ;

n_output_gate_instance
locals []
: (  name_of_instance  )?  lparen  output_terminal (  comma  output_terminal  )*  comma  input_terminal  rparen ;

pass_switch_instance
locals []
: (  name_of_instance  )?  lparen  inout_terminal  comma  inout_terminal  rparen ;

pass_enable_switch_instance
locals []
: (  name_of_instance  )?  lparen  inout_terminal  comma  inout_terminal  comma  enable_terminal  rparen ;

pull_gate_instance
locals []
: (  name_of_instance  )?  lparen  output_terminal  rparen ;

pulldown_strength
locals []
:  lparen  strength0  comma  strength1  rparen 
|  lparen  strength1  comma  strength0  rparen 
|  lparen  strength0  rparen ;

pullup_strength
locals []
:  lparen  strength0  comma  strength1  rparen 
|  lparen  strength1  comma  strength0  rparen 
|  lparen  strength1  rparen ;

enable_terminal
locals []
:  expression ;

inout_terminal
locals []
:  net_lvalue ;

input_terminal
locals []
:  expression ;

ncontrol_terminal
locals []
:  expression ;

output_terminal
locals []
:  net_lvalue ;

pcontrol_terminal
locals []
:  expression ;

cmos_switchtype
locals []
:  cmosstr 
|  rcmosstr ;

enable_gatetype
locals []
:  bufif0str 
|  bufif1str 
|  notif0str 
|  notif1str ;

mos_switchtype
locals []
:  nmosstr 
|  pmos 
|  rnmosstr 
|  rpmosstr ;

n_input_gatetype
locals []
:  andstr 
|  nandstr 
|  orstr 
|  norstr 
|  xorstrstr 
|  xnorstr ;

n_output_gatetype
locals []
:  bufstr 
|  notstr ;

pass_en_switchtype
locals []
:  tranif0str 
|  tranif1str 
|  rtranif1str 
|  rtranif0str ;

pass_switchtype
locals []
:  transtr 
|  rtranstr ;

module_instantiation
locals []
:  module_identifier (  parameter_value_assignment  )?  hierarchical_instance (  comma  hierarchical_instance  )*  semi pragma_def? ;

parameter_value_assignment
locals []
:  hash  lparen (  list_of_parameter_assignments  )?  rparen ;

list_of_parameter_assignments
locals []
:  ordered_parameter_assignment (  comma  ordered_parameter_assignment  )* 
|  named_parameter_assignment (  comma  named_parameter_assignment  )* ;

ordered_parameter_assignment
locals []
:  param_expression ;

named_parameter_assignment
locals []
:  dot  parameter_identifier  lparen (  param_expression  )?  rparen ;

hierarchical_instance
locals []
:  name_of_instance  lparen (  list_of_port_connections  )?  rparen ;

name_of_instance
locals []
:  instance_identifier (  unpacked_dimension  )* ;

list_of_port_connections
locals []
:  ordered_port_connection (  comma  ordered_port_connection  )* 
|  named_port_connection (  comma  named_port_connection  )* ;

ordered_port_connection
locals []
:  attribute_instance* (  expression  )? ;

named_port_connection
locals []
:  attribute_instance*  dot  port_identifier (  lparen (  expression  )?  rparen  )? 
|  attribute_instance*  dotstar ;

interface_instantiation
locals []
:  interface_identifier (  parameter_value_assignment  )?  hierarchical_instance (  comma  hierarchical_instance  )*  semi ;

program_instantiation
locals []
:  program_identifier (  parameter_value_assignment  )?  hierarchical_instance (  comma  hierarchical_instance  )*  semi ;

checker_instantiation
locals []
:  checker_identifier  name_of_instance  lparen (  list_of_checker_port_connections  )?  rparen  semi ;

list_of_checker_port_connections
locals []
:  ordered_checker_port_connection (  comma  ordered_checker_port_connection  )* 
|  named_checker_port_connection (  comma  named_checker_port_connection  )* ;

ordered_checker_port_connection
locals []
:  attribute_instance* (  property_actual_arg  )? ;

named_checker_port_connection
locals []
:  attribute_instance*  dot  port_identifier (  lparen (  property_actual_arg  )?  rparen  )? 
|  attribute_instance*  dotstar ;

generate_region
locals []
:  generatestr (  generate_block  )*  endgeneratestr ;

loop_generate_construct
locals []
:  forstr  lparen  genvar_initialization  semi  genvar_expression  semi  genvar_iteration  rparen  generate_block pragma_def?;

genvar_initialization
locals []
: (  genvarstr  )?  genvar_identifier  assign  constant_expression ;

conditional_generate_construct
locals []
:  if_generate_construct 
|  case_generate_construct ;

if_generate_construct
locals []
:  ifstr  lparen  constant_expression  rparen  generate_block (  elsestr  generate_block  )? ;

case_generate_construct
locals []
:  casestr  lparen  constant_expression  rparen  case_generate_item (  case_generate_item  )*  endcasestr ;

case_generate_item
locals []
:  constant_expression (  comma  constant_expression  )*  colon  generate_block 
|  defaultstr (  colon  )?  generate_block ;

generate_block
locals []
:  generate_item 
| (  generate_block_identifier  colon  )?  generate_block_part1  generate_block_part3*  generate_block_part2 ;

generate_block_part1
locals []
:  beginstr (  colon  generate_block_identifier  )? ;

generate_block_part2
locals []
:  endstr (  colon  generate_block_identifier  )? ;

generate_block_part3
locals []
:  generate_item 
|  generate_block ;

generate_item
locals []
:  module_or_generate_item 
|  interface_or_generate_item 
|  checker_or_generate_item ;

udp_nonansi_declaration
locals []
:  attribute_instance*  primitivestr  udp_identifier  lparen  udp_port_list  rparen  semi ;

genvar_iteration
locals []
:  genvar_identifier  assignment_operator  genvar_expression 
|  inc_or_dec_operator  genvar_identifier 
|  genvar_identifier  inc_or_dec_operator ;

udp_ansi_declaration
locals []
:  attribute_instance*  primitivestr  udp_identifier  lparen  udp_declaration_port_list  rparen  semi ;

udp_declaration
locals []
:  udp_nonansi_declaration  udp_port_declaration (  udp_port_declaration  )*  udp_body  endprimitivestr (  colon  udp_identifier  )? 
|  udp_ansi_declaration  udp_body  endprimitivestr (  colon  udp_identifier  )? 
|  externstr  udp_nonansi_declaration 
|  externstr  udp_ansi_declaration 
|  attribute_instance*  primitivestr  udp_identifier  lparen  dotstar  rparen  semi (  udp_port_declaration  )*  udp_body  endprimitivestr (  colon  udp_identifier  )? ;

udp_port_list
locals []
:  output_port_identifier  comma  input_port_identifier (  comma  input_port_identifier  )* ;

udp_declaration_port_list
locals []
:  udp_output_declaration  comma  udp_input_declaration (  comma  udp_input_declaration  )* ;

udp_port_declaration
locals []
:  udp_output_declaration  semi 
|  udp_input_declaration  semi 
|  udp_reg_declaration  semi ;

udp_output_declaration
locals []
:  attribute_instance*  outputstr (  regstr  )?  port_identifier (  assign  constant_expression  )? ;

udp_input_declaration
locals []
:  attribute_instance*  inputstr  list_of_udp_port_identifiers ;

udp_reg_declaration
locals []
:  attribute_instance*  regstr  variable_identifier ;

udp_body
locals []
:  combinational_body 
|  sequential_body ;

combinational_body
locals []
:  tablestr  combinational_entry (  combinational_entry  )*  endtablestr ;

combinational_entry
locals []
:  level_input_list  colon  output_symbol  semi ;

sequential_body
locals []
: (  udp_initial_statement  )?  tablestr  sequential_entry (  sequential_entry  )*  endtablestr ;

udp_initial_statement
locals []
:  initialstr  output_port_identifier  assign  init_val  semi ;

init_val
locals []
:  binary_number 
|  zero_or_one 
|  edge_spec ;

sequential_entry
locals []
:  seq_input_list  colon  current_state  colon  next_state  semi ;

seq_input_list
locals []
:  level_input_list 
|  edge_input_list ;

level_input_list
locals []
:  level_symbol (  level_symbol  )* ;

edge_input_list
locals []
:  edge_input_list_part1 (  level_symbol  )* ;

edge_input_list_part1
locals []
: (  level_symbol  )*  edge_indicator ;

edge_indicator
locals []
:  lparen  level_symbol  level_symbol  rparen 
|  edge_symbol ;

current_state
locals []
:  level_symbol ;

next_state
locals []
:  output_symbol 
|  minus ;

output_symbol
locals []
:  binary_number 
|  edge_spec 
|  zero_or_one ;

level_symbol
locals []
:  binary_number 
|  edge_spec 
|  zero_or_one 
|  questinmark 
|  id ;

edge_symbol
locals []
:  id 
|  star ;

udp_instantiation
locals []
:  udp_identifier (  drive_strength  )? (  delay2  )?  udp_instance (  comma  udp_instance  )*  semi ;

udp_instance
locals []
: (  name_of_instance  )?  lparen  output_terminal  comma  input_terminal (  comma  input_terminal  )*  rparen ;

continuous_assign
locals []
:  assignstrstr (  drive_strength  )? (  delay3  )? (  list_of_net_assignments  ) semi ;

list_of_net_assignments
locals []
:  net_assignment (  comma  net_assignment  )* ;

list_of_variable_assignments
locals []
:  variable_assignment (  comma  variable_assignment  )* ;

net_alias
locals []
:  aliasstr  net_lvalue  assign  net_lvalue (  assign  net_lvalue  )*  semi ;

net_assignment
locals []
:  net_lvalue  assign  expression ;

initial_construct
locals []
:  initialstr  statement_or_null ;

always_construct
locals []
:  always_keyword  statement ;

always_keyword
locals []
:  alwaysstr 
|  always_combstr 
|  always_latchstr 
|  always_ffstr ;

final_construct
locals []
:  finalstr  function_statement ;

blocking_assignment
locals []
:  variable_lvalue  assign  delay_or_event_control  expression 
|  nonrange_variable_lvalue  assign  dynamic_array_new 
| (  implicit_class_handle  dot  |  class_scope  |  package_scope  )?  hierarchical_variable_identifier  select  assign  class_new 
|  operator_assignment ;

operator_assignment
locals []
:  variable_lvalue  assignment_operator  expression ;

assignment_operator
locals []
:  assign 
|  plusequals 
|  minusequals 
|  startequals 
|  slashequals 
|  percentileequal 
|  andequals 
|  orequal 
|  xorequal 
|  lshift_assign 
|  rshift_assign 
|  unsigned_lshift_assign 
|  unsigned_rshift_assign ;

nonblocking_assignment
locals []
:  variable_lvalue  le (  delay_or_event_control  )?  expression ;

procedural_continuous_assignment
locals []
:  assignstrstr  variable_assignment 
|  deassignstr  variable_lvalue 
|  forcestr  variable_assignment 
|  forcestr  net_assignment 
|  releasestr  variable_lvalue 
|  releasestr  net_lvalue ;

action_block
locals []
:  statement_or_null 
| (  statement  )?  elsestr  statement_or_null ;

seq_block
locals []
:  seq_block_part1 (  block_item_declaration  )* (  statement_or_null  )*  endstr (  colon  block_identifier  )? ;

seq_block_part1
locals []
:  beginstr (  colon  block_identifier  )? ;

par_block
locals []
:  par_block_part1 (  block_item_declaration  )* (  statement_or_null  )*  join_keyword (  colon  block_identifier  )? ;

par_block_part1
locals []
:  forkstr (  colon  block_identifier  )? ;

join_keyword
locals []
:  joinstr 
|  join_anystr 
|  join_namestr ;

statement_or_null
locals []
:  statement 
|  attribute_instance*  semi ;

statement
locals []
: (  block_identifier  colon  )?  attribute_instance*  statement_item ;

statement_item
locals []
:  blocking_assignment  semi 
|  nonblocking_assignment  semi 
|  procedural_continuous_assignment  semi 
|  case_statement 
|  conditional_statement 
|  inc_or_dec_expression  semi 
|  subroutine_call_statement 
|  disable_statement 
|  event_trigger 
|  loop_statement 
|  jump_statement 
|  par_block 
|  procedural_timing_control_statement 
|  seq_block 
|  wait_statement 
|  procedural_assertion_statement 
|  clocking_drive  semi 
|  randsequence_statement 
|  randcase_statement 
|  expect_property_statement ;

function_statement
locals []
:  statement ;

function_statement_or_null
locals []
:  function_statement 
|  attribute_instance*  semi ;

variable_identifier_list
locals []
:  variable_identifier (  comma  variable_identifier  )* ;

procedural_timing_control_statement
locals []
:  procedural_timing_control  statement_or_null ;

delay_or_event_control
locals []
:  delay_control 
|  event_control 
|  repeatstr  lparen  expression  rparen  event_control ;

delay_control
locals []
:  hash  delay_value 
|  hash  lparen  mintypmax_expression  rparen ;

event_control
locals []
:  attherate  hierarchical_event_identifier 
|  attherate  lparen  event_expression  rparen 
|  attheratestar 
|  attherate  lparenstarrparen 
|  attherate  ps_or_hierarchical_sequence_identifier ;

event_expression
locals []
: ( (  edge_identifier  )?  expression (  iffstr  expression  )?  |  sequence_instance (  iffstr  expression  )?  |  lparen  event_expression  rparen  )(  orstr  event_expression  |  comma  event_expression  )* ;

procedural_timing_control
locals []
:  delay_control 
|  event_control 
|  cycle_delay ;

jump_statement
locals []
:  returnstr (  expression  )?  semi 
|  breakstr  semi 
|  continuestr  semi ;

wait_statement
locals []
:  waitstr  lparen  expression  rparen  statement_or_null 
|  waitstr  forkstr  semi 
|  wait_orderstr  lparen  hierarchical_identifier (  comma  hierarchical_identifier  )*  rparen  action_block ;

event_trigger
locals []
:  derive  hierarchical_event_identifier  semi 
|  derivegt (  delay_or_event_control  )?  hierarchical_event_identifier  semi ;

disable_statement
locals []
:  disablestr  hierarchical_task_identifier  semi 
|  disablestr  hierarchical_block_identifier  semi 
|  disablestr  forkstr  semi ;

conditional_statement
locals []
: (  unique_priority  )?  ifstr  lparen  expression  rparen  statement_or_null (  elsestr  ifstr  lparen  expression  rparen  statement_or_null  )* (  elsestr  statement_or_null  )? ;

unique_priority
locals []
:  uniquestr 
|  unique0str 
|  prioritystr ;

case_statement
locals []
: (  unique_priority  )?  case_keyword  lparen  case_expression  rparen  case_item (  case_item  )*  endcasestr 
| (  unique_priority  )?  case_keyword  lparen  case_expression  rparen  matchesstr  case_pattern_item (  case_pattern_item  )*  endcasestr 
| (  unique_priority  )?  casestr  lparen  case_expression  rparen  insidestr  case_inside_item (  case_inside_item  )*  endcasestr ;

case_keyword
locals []
:  casestr 
|  casezstr 
|  casexstr ;

case_expression
locals []
:  expression ;

case_item
locals []
:  case_item_expression (  comma  case_item_expression  )*  colon  statement_or_null 
|  defaultstr (  colon  )?  statement_or_null ;

case_pattern_item
locals []
:  pattern (  andandand  expression  )?  colon  statement_or_null 
|  defaultstr (  colon  )?  statement_or_null ;

case_inside_item
locals []
:  open_range_list  colon  statement_or_null 
|  defaultstr (  colon  )?  statement_or_null ;

case_item_expression
locals []
:  expression ;

randcase_statement
locals []
:  randcasestr  randcase_item (  randcase_item  )*  endcasestr ;

randcase_item
locals []
:  expression  colon  statement_or_null ;

pattern
locals []
:  dot  variable_identifier 
|  dotstar 
|  constant_expression 
|  taggedstr  member_identifier (  pattern  )? 
|  escapelcurl  pattern (  comma  pattern  )*  rcurl 
|  escapelcurl  member_identifier  colon  pattern (  comma  member_identifier  colon  pattern  )*  rcurl ;

assignment_pattern
locals []
:  escapelcurl  expression (  comma  expression  )*  rcurl 
|  escapelcurl  structure_pattern_key  colon  expression (  comma  structure_pattern_key  colon  expression  )*  rcurl 
|  escapelcurl  array_pattern_key  colon  expression (  comma  array_pattern_key  colon  expression  )*  rcurl 
|  escapelcurl  constant_expression  lcurl  expression (  comma  expression  )*  rcurl  rcurl ;

structure_pattern_key
locals []
:  member_identifier 
|  assignment_pattern_key ;

array_pattern_key
locals []
:  constant_expression 
|  assignment_pattern_key ;

assignment_pattern_key
locals []
:  simple_type 
|  defaultstr ;

variable_assignment
locals []
:  variable_lvalue  assign  expression ;

assignment_pattern_expression
locals []
: (  assignment_pattern_expression_type  )?  assignment_pattern ;

assignment_pattern_expression_type
locals []
:  ps_type_identifier 
|  ps_parameter_identifier 
|  integer_atom_type 
|  type_reference ;

constant_assignment_pattern_expression
locals []
:  assignment_pattern_expression ;

assignment_pattern_net_lvalue
locals []
:  escapelcurl  net_lvalue (  comma  net_lvalue  )*  rcurl ;

assignment_pattern_variable_lvalue
locals []
:  escapelcurl  variable_lvalue (  comma  variable_lvalue  )*  rcurl ;

loop_statement
locals []
:  foreverstr  statement_or_null 
|  repeatstr  lparen  expression  rparen  statement_or_null 
|  whilestr  lparen  expression  rparen  statement_or_null 
|  forstr  lparen  for_initialization  semi  expression  semi  for_step  rparen  statement_or_null 
|  dostr  statement_or_null  whilestr  lparen  expression  rparen  semi 
|  foreachstr  lparen  ps_or_hierarchical_array_identifier (  loop_variables  )?  rparen  statement ;

for_initialization
locals []
:  list_of_variable_assignments 
|  for_variable_declaration (  comma  for_variable_declaration  )* ;

for_variable_declaration
locals []
:  data_type  variable_identifier  assign  expression (  comma  variable_identifier  assign  expression  )* ;

for_step
locals []
:  for_step_assignment (  comma  for_step_assignment  )* ;

for_step_assignment
locals []
:  operator_assignment 
|  inc_or_dec_expression 
|  function_subroutine_call ;

loop_variables
locals []
: (  index_variable_identifier  )?  loop_variables_part1* ;

loop_variables_part1
locals []
:  comma (  index_variable_identifier  )? ;

subroutine_call_statement
locals []
:  subroutine_call  semi 
|  voidstr  escapequote  lparen  function_subroutine_call  rparen  semi ;

assertion_item
locals []
:  concurrent_assertion_item 
|  deferred_immediate_assertion_item ;

deferred_immediate_assertion_item
locals []
: (  block_identifier  colon  )?  deferred_immediate_assertion_statement ;

procedural_assertion_statement
locals []
:  concurrent_assertion_statement 
|  immediate_assertion_statement 
|  checker_instantiation ;

immediate_assertion_statement
locals []
:  simple_immediate_assertion_statement 
|  deferred_immediate_assertion_statement ;

simple_immediate_assertion_statement
locals []
:  simple_immediate_assert_statement 
|  simple_immediate_assume_statement 
|  simple_immediate_cover_statement ;

simple_immediate_assert_statement
locals []
:  assertstr  lparen  expression  rparen  action_block ;

simple_immediate_assume_statement
locals []
:  assumestr  lparen  expression  rparen  action_block ;

simple_immediate_cover_statement
locals []
:  coverstr  lparen  expression  rparen  statement_or_null ;

deferred_immediate_assertion_statement
locals []
:  deferred_immediate_assert_statement 
|  deferred_immediate_assume_statement 
|  deferred_immediate_cover_statement ;

deferred_immediate_assert_statement
locals []
:  assertstr  hash_zero  lparen  expression  rparen  action_block ;

deferred_immediate_assume_statement
locals []
:  assumestr  hash_zero  lparen  expression  rparen  action_block ;

deferred_immediate_cover_statement
locals []
:  coverstr  hash_zero  lparen  expression  rparen  action_block  statement_or_null ;

clocking_declaration
locals []
: (  defaultstr  )?  clocking_declaration_part1  clocking_event  semi (  clocking_item  )*  endclockingstr (  colon  clocking_identifier  )? 
|  globalstr  clocking_declaration_part1  clocking_event  semi  endclockingstr (  colon  clocking_identifier  )? ;

clocking_declaration_part1
locals []
:  clockingstr (  clocking_identifier  )? ;

clocking_event
locals []
:  attherate  identifier 
|  attherate  lparen  event_expression  rparen ;

clocking_item
locals []
:  defaultstr  default_skew  semi 
|  clocking_direction  list_of_clocking_decl_assign  semi 
|  attribute_instance*  assertion_item_declaration ;

default_skew
locals []
:  inputstr  clocking_skew 
|  outputstr  clocking_skew 
|  inputstr  clocking_skew  outputstr  clocking_skew ;

clocking_direction
locals []
:  inputstr (  clocking_skew  )? 
|  clocking_direction_part1 
|  inputstr (  clocking_skew  )?  clocking_direction_part1 
|  inoutstr ;

clocking_direction_part1
locals []
:  outputstr (  clocking_skew  )? ;

list_of_clocking_decl_assign
locals []
:  clocking_decl_assign (  comma  clocking_decl_assign  )* ;

clocking_decl_assign
locals []
:  signal_identifier (  assign  expression  )? ;

clocking_skew
locals []
:  edge_identifier (  delay_control  )? 
|  delay_control ;

clocking_drive
locals []
:  clockvar_expression  le (  cycle_delay  )?  expression ;

cycle_delay
locals []
:  double_hash (  zero_or_one  |  edge_spec  |  decimal_number  )
|  double_hash  identifier 
|  double_hash  lparen  expression  rparen ;

clockvar
locals []
:  hierarchical_identifier ;

clockvar_expression
locals []
:  clockvar  select ;

randsequence_statement
locals []
:  randsequencestr  lparen (  production_identifier  )?  rparen  production (  production  )*  endsequencestr ;

production
locals []
: (  data_type_or_void  )?  production_identifier (  lparen  tf_port_list  rparen  )?  colon  rs_rule (  or  rs_rule  )*  semi ;

rs_rule
locals []
:  rs_production_list (  colonequals  weight_specification (  rs_code_block  )?  )? ;

rs_production_list
locals []
:  rs_prod (  rs_prod  )* 
|  randstr  joinstr (  lparen  expression  rparen  )?  production_item  production_item (  production_item  )* ;

weight_specification
locals []
: (  zero_or_one  |  edge_spec  |  decimal_number  )
|  ps_identifier 
|  lparen  expression  rparen ;

rs_code_block
locals []
:  lcurl (  data_declaration  )* (  statement_or_null  )*  rcurl ;

rs_prod
locals []
:  production_item 
|  rs_code_block 
|  rs_if_else 
|  rs_repeat 
|  rs_case ;

production_item
locals []
:  production_identifier (  lparen  list_of_arguments  rparen  )? ;

rs_if_else
locals []
:  ifstr  lparen  expression  rparen  production_item (  elsestr  production_item  )? ;

rs_repeat
locals []
:  repeatstr  lparen  expression  rparen  production_item ;

rs_case
locals []
:  casestr  lparen  case_expression  rparen  rs_case_item (  rs_case_item  )*  endcasestr ;

rs_case_item
locals []
:  case_item_expression (  comma  case_item_expression  )*  colon  production_item  semi 
|  defaultstr (  colon  )?  production_item  semi ;

specify_block
locals []
:  specifystr  specify_item*  endspecifystr ;

specify_item
locals []
:  specparam_declaration 
|  pulsestyle_declaration 
|  showcancelled_declaration 
|  path_declaration 
|  system_timing_check ;

pulsestyle_declaration
locals []
:  pulsestyle_oneventstr  list_of_path_outputs  semi 
|  pulsestyle_ondetectstr  list_of_path_outputs  semi ;

showcancelled_declaration
locals []
:  showcancelledstr  list_of_path_outputs  semi 
|  noshowcancelledstr  list_of_path_outputs  semi ;

path_declaration
locals []
:  simple_path_declaration  semi 
|  edge_sensitive_path_declaration  semi 
|  state_dependent_path_declaration  semi ;

simple_path_declaration
locals []
:  parallel_path_description  assign  path_delay_value 
|  full_path_description  assign  path_delay_value ;

parallel_path_description
locals []
:  lparen  specify_input_terminal_descriptor  polarity_operator?  implies  specify_output_terminal_descriptor  rparen ;

full_path_description
locals []
:  lparen  list_of_path_inputs  polarity_operator?  stargt  list_of_path_outputs  rparen ;

list_of_path_inputs
locals []
:  specify_input_terminal_descriptor (  comma  specify_input_terminal_descriptor  )* ;

list_of_path_outputs
locals []
:  specify_output_terminal_descriptor (  comma  specify_output_terminal_descriptor  )* ;

specify_input_terminal_descriptor
locals []
:  input_identifier (  lbrack  constant_range_expression  rbrack  )? ;

specify_output_terminal_descriptor
locals []
:  output_identifier (  lbrack  constant_range_expression  rbrack  )? ;

input_identifier
locals []
:  input_port_identifier 
|  inout_port_identifier 
|  interface_identifier  dot  port_identifier ;

output_identifier
locals []
:  output_port_identifier 
|  inout_port_identifier 
|  interface_identifier  dot  port_identifier ;

path_delay_value
locals []
:  list_of_path_delay_expressions 
|  lparen  list_of_path_delay_expressions  rparen ;

list_of_path_delay_expressions
locals []
:  t_path_delay_expression 
|  trise_path_delay_expression  comma  tfall_path_delay_expression 
|  trise_path_delay_expression  comma  tfall_path_delay_expression  comma  tz_path_delay_expression 
|  t01_path_delay_expression  comma  t10_path_delay_expression  comma  t0z_path_delay_expression  comma  tz1_path_delay_expression  comma  t1z_path_delay_expression  comma  tz0_path_delay_expression 
|  t01_path_delay_expression  comma  t10_path_delay_expression  comma  t0z_path_delay_expression  comma  tz1_path_delay_expression  comma  t1z_path_delay_expression  comma  tz0_path_delay_expression  comma  t0x_path_delay_expression  comma  tx1_path_delay_expression  comma  t1x_path_delay_expression  comma  tx0_path_delay_expression  comma  txz_path_delay_expression  comma  tzx_path_delay_expression ;

t_path_delay_expression
locals []
:  path_delay_expression ;

trise_path_delay_expression
locals []
:  path_delay_expression ;

tfall_path_delay_expression
locals []
:  path_delay_expression ;

tz_path_delay_expression
locals []
:  path_delay_expression ;

t01_path_delay_expression
locals []
:  path_delay_expression ;

t10_path_delay_expression
locals []
:  path_delay_expression ;

t0z_path_delay_expression
locals []
:  path_delay_expression ;

tz1_path_delay_expression
locals []
:  path_delay_expression ;

t1z_path_delay_expression
locals []
:  path_delay_expression ;

tz0_path_delay_expression
locals []
:  path_delay_expression ;

t0x_path_delay_expression
locals []
:  path_delay_expression ;

tx1_path_delay_expression
locals []
:  path_delay_expression ;

t1x_path_delay_expression
locals []
:  path_delay_expression ;

tx0_path_delay_expression
locals []
:  path_delay_expression ;

txz_path_delay_expression
locals []
:  path_delay_expression ;

tzx_path_delay_expression
locals []
:  path_delay_expression ;

path_delay_expression
locals []
:  constant_mintypmax_expression ;

edge_sensitive_path_declaration
locals []
:  parallel_edge_sensitive_path_description  assign  path_delay_value 
|  full_edge_sensitive_path_description  assign  path_delay_value ;

parallel_edge_sensitive_path_description
locals []
:  lparen  edge_identifier?  specify_input_terminal_descriptor  implies  lparen  specify_output_terminal_descriptor  polarity_operator?  colon  data_source_expression  rparen  rparen ;

full_edge_sensitive_path_description
locals []
:  lparen  edge_identifier?  list_of_path_inputs  stargt  lparen  list_of_path_outputs  polarity_operator?  colon  data_source_expression  rparen  rparen ;

data_source_expression
locals []
:  expression ;

edge_identifier
locals []
:  posedgestr 
|  negedgestr 
|  edgestr ;

state_dependent_path_declaration
locals []
:  ifstr  lparen  module_path_expression  rparen  simple_path_declaration 
|  ifstr  lparen  module_path_expression  rparen  edge_sensitive_path_declaration 
|  ifnonestr  simple_path_declaration ;

polarity_operator
locals []
:  plus 
|  minus ;

system_timing_check
locals []
:  setup_timing_check 
|  hold_timing_check 
|  setuphold_timing_check 
|  recovery_timing_check 
|  removal_timing_check 
|  recrem_timing_check 
|  skew_timing_check 
|  timeskew_timing_check 
|  fullskew_timing_check 
|  period_timing_check 
|  width_timing_check 
|  nochange_timing_check ;

setup_timing_check
locals []
:  dollarsetupstr  lparen  data_event  comma  reference_event  comma  timing_check_limit (  comma  notifier  )?  rparen  semi ;

hold_timing_check
locals []
:  dollarholdstr  lparen  reference_event  comma  data_event  comma  timing_check_limit (  comma  notifier  )?  rparen  semi ;

setuphold_timing_check
locals []
:  dollarsetupholdstr  lparen  reference_event  comma  data_event  comma  timing_check_limit  comma  timing_check_limit (  comma  notifier? (  comma  timestamp_condition? (  comma  timecheck_condition? (  comma  delayed_reference? (  comma  delayed_data?  )?  )?  )?  )?  )?  rparen  semi ;

recovery_timing_check
locals []
:  dollarrecoverystr  lparen  reference_event  comma  data_event  comma  timing_check_limit (  comma  notifier  )?  rparen  semi ;

removal_timing_check
locals []
:  dollarremovalstr  lparen  reference_event  comma  data_event  comma  timing_check_limit (  comma  notifier  )?  rparen  semi ;

recrem_timing_check
locals []
:  dollarrecremstr  lparen  reference_event  comma  data_event  comma  timing_check_limit  comma  timing_check_limit (  comma  notifier? (  comma  timestamp_condition? (  comma  timecheck_condition? (  comma  delayed_reference? (  comma  delayed_data?  )?  )?  )?  )?  )?  rparen  semi ;

skew_timing_check
locals []
:  dollarskewstr  lparen  reference_event  comma  data_event  comma  timing_check_limit (  comma  notifier  )?  rparen  semi ;

timeskew_timing_check
locals []
:  dollartimeskewstr  lparen  reference_event  comma  data_event  comma  timing_check_limit (  comma  notifier? (  comma  event_based_flag? (  comma  remain_active_flag?  )?  )?  )?  rparen  semi ;

fullskew_timing_check
locals []
:  dollarfullskewstr  lparen  reference_event  comma  data_event  comma  timing_check_limit  comma  timing_check_limit (  comma  notifier (  comma  event_based_flag (  comma  remain_active_flag  )?  )?  )?  rparen  semi ;

period_timing_check
locals []
:  dollarperiodstr  lparen  controlled_reference_event  comma  timing_check_limit (  comma  notifier  )?  rparen  semi ;

width_timing_check
locals []
:  dollaewidthstr  lparen  controlled_reference_event  comma  timing_check_limit  comma  threshold (  comma  notifier  )?  rparen  semi ;

nochange_timing_check
locals []
:  dollarnochangestr  lparen  reference_event  comma  data_event  comma  start_edge_offset  comma  end_edge_offset (  comma  notifier  )?  rparen  semi ;

timecheck_condition
locals []
:  mintypmax_expression ;

controlled_reference_event
locals []
:  controlled_timing_check_event ;

data_event
locals []
:  timing_check_event ;

delayed_data
locals []
:  terminal_identifier 
|  terminal_identifier  lbrack  constant_mintypmax_expression  rbrack ;

delayed_reference
locals []
:  terminal_identifier 
|  terminal_identifier  lbrack  constant_mintypmax_expression  rbrack ;

end_edge_offset
locals []
:  mintypmax_expression ;

event_based_flag
locals []
:  constant_expression ;

notifier
locals []
:  variable_identifier ;

reference_event
locals []
:  timing_check_event ;

remain_active_flag
locals []
:  constant_mintypmax_expression ;

timestamp_condition
locals []
:  mintypmax_expression ;

start_edge_offset
locals []
:  mintypmax_expression ;

threshold
locals []
:  constant_expression ;

timing_check_limit
locals []
:  expression ;

timing_check_event
locals []
:  timing_check_event_control?  specify_terminal_descriptor (  andandand  timing_check_condition  )? ;

controlled_timing_check_event
locals []
:  timing_check_event_control  specify_terminal_descriptor (  andandand  timing_check_condition  )? ;

timing_check_event_control
locals []
:  posedgestr 
|  negedgestr 
|  edgestr 
|  edge_control_specifier ;

specify_terminal_descriptor
locals []
:  specify_input_terminal_descriptor 
|  specify_output_terminal_descriptor ;

edge_control_specifier
locals []
:  edgestr  lbrack  edge_descriptor (  comma  edge_descriptor  )*  rbrack ;

edge_descriptor
locals []
:  edge_spec 
|  z_or_x  zero_or_one 
|  zero_or_one  z_or_x ;

timing_check_condition
locals []
:  scalar_timing_check_condition 
|  lparen  scalar_timing_check_condition  rparen ;

scalar_timing_check_condition
locals []
:  expression 
|  compliment  expression 
|  expression  equals  scalar_constant 
|  expression  case_equality  scalar_constant 
|  expression  not_equals  scalar_constant 
|  expression  case_inequality  scalar_constant ;

scalar_constant
locals []
:  binary_number 
|  edge_spec 
|  zero_or_one ;

concatenation
locals []
:  lcurl  expression (  comma  expression  )*  rcurl ;

constant_concatenation
locals []
:  lcurl  constant_expression (  comma  constant_expression  )*  rcurl ;

constant_multiple_concatenation
locals []
:  lcurl  constant_expression  constant_concatenation  rcurl ;

module_path_concatenation
locals []
:  lcurl  module_path_expression (  comma  module_path_expression  )*  rcurl ;

module_path_multiple_concatenation
locals []
:  lcurl  constant_expression  module_path_concatenation  rcurl ;

multiple_concatenation
locals []
:  lcurl  expression  concatenation  rcurl ;

streaming_concatenation
locals []
:  lcurl  stream_operator  slice_size?  stream_concatenation  rcurl ;

stream_operator
locals []
:  rshift 
|  lshift ;

slice_size
locals []
:  simple_type 
|  constant_expression ;

stream_concatenation
locals []
:  lcurl  stream_expression (  comma  stream_expression  )*  rcurl ;

stream_expression
locals []
:  expression (  withstr  lbrack  array_range_expression  rbrack  )? ;

array_range_expression
locals []
:  expression 
|  expression  colon  expression 
|  expression  pluscolon  expression 
|  expression  minuscolon  expression ;

empty_queue
locals []
:  lcurl  rcurl ;

constant_function_call
locals []
:  function_subroutine_call ;

tf_call
locals []
:  ps_or_hierarchical_tf_identifier  attribute_instance? (  lparen  list_of_arguments  rparen  )? ;

system_tf_call
locals []
:  system_tf_identifier (  lparen  list_of_arguments  rparen  )? 
|  system_tf_identifier  lparen  data_type (  comma  expression  )?  rparen ;

subroutine_call
locals []
:  tf_call 
|  system_tf_call 
|  method_call 
| (  stdcoloncolon  )?  randomize_call ;

function_subroutine_call
locals []
:  subroutine_call ;

list_of_arguments
locals []
:  expression?  list_of_arguments_part1* (  comma  list_of_arguments_part2  )* 
|  list_of_arguments_part2 (  comma  list_of_arguments_part2  )* ;

list_of_arguments_part1
locals []
:  comma  expression? ;

list_of_arguments_part2
locals []
:  dot  identifier  lparen  expression?  rparen ;

method_call
locals []
:  method_call_root  dot  method_call_body ;

method_call_body
locals []
:  method_identifier  attribute_instance? (  lparen  list_of_arguments  rparen  )? 
|  built_in_method_call ;

built_in_method_call
locals []
:  array_manipulation_call 
|  randomize_call ;

array_manipulation_call
locals []
:  array_method_name  attribute_instance? (  lparen  list_of_arguments  rparen  )? (  withstr  lparen  expression  rparen  )? ;

randomize_call
locals []
:  randomizestr  attribute_instance? (  lparen (  variable_identifier_list  |  nullstr  )?  rparen  )? (  withstr (  lparen  identifier_list?  rparen  )?  constraint_block  )? ;

method_call_root
locals []
:  primary_no_function_call 
|  implicit_class_handle ;

array_method_name
locals []
:  method_identifier 
|  uniquestr 
|  andstr 
|  orstr 
|  xorstrstr ;

inc_or_dec_expression
locals []
:  inc_or_dec_expression_part1  variable_lvalue 
|  inc_or_dec_expression_part2  inc_or_dec_operator ;

inc_or_dec_expression_part1
locals []
:  inc_or_dec_operator  attribute_instance? ;

inc_or_dec_expression_part2
locals []
:  variable_lvalue  attribute_instance? ;

constant_expression
locals []
:  constant_primary #const_expr_only_primary
|  unary_operator  attribute_instance*  constant_primary #const_expr_unary_op
|  constant_expression  starstar  attribute_instance*  constant_expression #const_expr_st_st
|  constant_expression (  star  |  div  |  modulo  ) attribute_instance*  constant_expression #const_expr_mutl
|  constant_expression (  plus  |  minus  ) attribute_instance*  constant_expression #const_expr_add
|  constant_expression (  lshift  |  rshift  |  alshift  |  arshift  ) attribute_instance*  constant_expression #const_expr_shift
|  constant_expression (  lt  |  gt  |  le  |  ge  ) attribute_instance*  constant_expression #const_expr_comp
|  constant_expression (  equals  |  not_equals  |  case_equality  |  case_inequality  |  case_q  |  not_case_q  ) attribute_instance*  constant_expression #const_expr_equality
|  constant_expression (  and  ) attribute_instance*  constant_expression #const_expr_binary_and
|  constant_expression (  xor  |  xnor  |  xorn  ) attribute_instance*  constant_expression #const_expr_binary_xor
|  constant_expression (  or  ) attribute_instance*  constant_expression #const_expr_binary_or
|  constant_expression (  log_and  ) attribute_instance*  constant_expression #const_expr_log_and
|  constant_expression (  log_or  ) attribute_instance*  constant_expression #const_expr_log_or
|  constant_expression  questinmark  attribute_instance*  constant_expression  colon  constant_expression #const_expr_conditional;

constant_mintypmax_expression
locals []
:  constant_expression 
|  constant_expression  colon  constant_expression  colon  constant_expression ;

constant_param_expression
locals []
:  constant_mintypmax_expression 
|  data_type 
|  dollar ;

param_expression
locals []
:  mintypmax_expression 
|  data_type ;

constant_range_expression
locals []
:  constant_expression 
|  constant_part_select_range ;

constant_part_select_range
locals []
:  constant_range 
|  constant_indexed_range ;

constant_range
locals []
:  constant_expression  colon  constant_expression ;

constant_indexed_range
locals []
:  constant_expression  pluscolon  constant_expression 
|  constant_expression  minuscolon  constant_expression ;

expr_
locals []
:  expression ;

expression
locals []
:  primary #expression_only_primary
|  unary_operator  attribute_instance*  primary #expression_unary_op
|  inc_or_dec_expression #expression_inc_or_dec
|  lparen  operator_assignment  rparen #expression_op_assign
|  expression  starstar  attribute_instance*  expression #expression_st_st
|  expression (  star  |  div  |  modulo  ) attribute_instance*  expression #expression_mult
|  expression (  plus  |  minus  ) attribute_instance*  expression #expression_add
|  expression (  lshift  |  rshift  |  alshift  |  arshift  ) attribute_instance*  expression #expression_shift
|  expression (  lt  |  gt  |  le  |  ge  ) attribute_instance*  expression #expression_comp
|  expression  insidestr  lcurl  open_range_list  rcurl #expression_inside_exp
|  expression (  equals  |  not_equals  |  case_equality  |  case_inequality  |  case_q  |  not_case_q  ) attribute_instance*  expression #expression_equality
|  expression (  and  ) attribute_instance*  expression #expression_binary_and
|  expression (  xor  |  xnor  |  xorn  ) attribute_instance*  expression #expression_binary_xor
|  expression (  or  ) attribute_instance*  expression #expression_binary_or
|  expression (  log_and  ) attribute_instance*  expression #expression_log_and
|  expression (  log_or  ) attribute_instance*  expression #expression_log_or
|  expression (  questinmark  attribute_instance*  expression  colon  expression  )#expression_conditional_exp
|  tagged_union_expression #expression_tagged_union;

matches_pattern
locals []
:  matchesstr  pattern ;

tagged_union_expression
locals []
:  taggedstr  member_identifier  expression? ;

value_range
locals []
:  expression 
|  lbrack  expression  colon  expression  rbrack ;

mintypmax_expression
locals []
:  expression 
|  expression  colon  expression  colon  expression ;

module_path_expression
locals []
:  module_path_conditional_expression ;

module_path_conditional_expression
locals []
:  module_path_binary_expression (  questinmark  attribute_instance?  module_path_expression  colon  module_path_expression  )? ;

module_path_binary_expression
locals []
:  module_path_unary_expression (  binary_module_path_operator  attribute_instance?  module_path_unary_expression  )? ;

module_path_unary_expression
locals []
:  unary_module_path_operator  attribute_instance?  module_path_primary ;

module_path_mintypmax_expression
locals []
:  module_path_expression 
|  module_path_expression  colon  module_path_expression  colon  module_path_expression ;

part_select_range
locals []
:  constant_range 
|  indexed_range ;

indexed_range
locals []
:  expression  pluscolon  constant_expression 
|  expression  minuscolon  constant_expression ;

genvar_expression
locals []
:  constant_expression ;

constant_primary
locals []
:  primary_literal 
|  ps_parameter_identifier  constant_select 
|  specparam_identifier (  lbrack  constant_range_expression  rbrack  )? 
|  genvar_identifier 
| (  package_scope  |  class_scope  )?  enum_identifier 
|  constant_concatenation (  lbrack  constant_range_expression  rbrack  )? 
|  constant_multiple_concatenation (  lbrack  constant_range_expression  rbrack  )? 
|  constant_function_call 
|  constant_let_expression 
|  lparen  constant_mintypmax_expression  rparen 
|  constant_cast 
|  constant_assignment_pattern_expression 
|  type_reference ;

module_path_primary
locals []
:  number 
|  identifier 
|  module_path_concatenation 
|  module_path_multiple_concatenation 
|  function_subroutine_call 
|  lparen  module_path_mintypmax_expression  rparen ;

primary_no_function_call
locals []
:  primary_literal 
| (  implicit_class_handle  dot  |  class_scope  |  package_scope  )?  hierarchical_identifier  select 
|  empty_queue 
|  concatenation (  lbrack  range_expression  rbrack  )? 
|  multiple_concatenation (  lbrack  range_expression  rbrack  )? 
|  let_expression 
|  lparen  mintypmax_expression  rparen 
|  cast 
|  assignment_pattern_expression 
|  streaming_concatenation 
|  sequence_method_call 
|  thisstr 
|  dollar 
|  nullstr ;

primary
locals []
:  primary_no_function_call 
|  function_subroutine_call ;

class_qualifier
locals []
: (  localcoloncolon  )? (  implicit_class_handle  dot  |  class_scope  )? ;

range_expression
locals []
:  expression 
|  part_select_range ;

primary_literal
locals []
:  number 
|  time_literal 
|  unbased_unsized_literal 
|  string_literal ;

time_literal
locals []
:  zero_or_one  time_unit 
|  edge_spec  time_unit 
|  decimal_number  time_unit 
|  real_number  time_unit ;

implicit_class_handle
locals []
:  thisstr  dot  superstr 
|  thisstr 
|  superstr ;

bit_select
locals []
: (  lbrack  expression  rbrack  )* ;

select
locals []
: ( (  dot  member_identifier  bit_select  )*  dot  member_identifier  )?  bit_select (  lbrack  part_select_range  rbrack  )? ;

nonrange_select
locals []
: ( (  dot  member_identifier  bit_select  )*  dot  member_identifier  )?  bit_select ;

constant_bit_select
locals []
: (  lbrack  constant_expression  rbrack  )* ;

constant_select
locals []
: ( (  dot  member_identifier  constant_bit_select  )*  dot  member_identifier  )?  constant_bit_select (  lbrack  constant_part_select_range  rbrack  )* ;

constant_cast
locals []
:  casting_type  escapequote  lparen  constant_expression  rparen ;

constant_let_expression
locals []
:  let_expression ;

cast
locals []
:  casting_type  escapequote  lparen  expression  rparen ;

net_lvalue
locals []
:  ps_or_hierarchical_net_identifier  constant_select 
|  lcurl  net_lvalue (  comma  net_lvalue  )*  rcurl 
|  lbrack  assignment_pattern_expression_type  rbrack  assignment_pattern_net_lvalue ;

variable_lvalue
locals []
: (  implicit_class_handle  dot  |  package_scope  )?  hierarchical_variable_identifier  select 
|  lcurl  variable_lvalue (  comma  variable_lvalue  )*  rcurl 
| (  assignment_pattern_expression_type  )?  assignment_pattern_variable_lvalue 
|  streaming_concatenation ;

nonrange_variable_lvalue
locals []
: (  implicit_class_handle  dot  |  package_scope  )?  hierarchical_variable_identifier  nonrange_select ;

unary_operator
locals []
:  plus 
|  minus 
|  not 
|  compliment 
|  and 
|  nand 
|  or 
|  nor 
|  xor 
|  xorn 
|  xnor ;

binary_operator
locals []
:  plus 
|  minus 
|  star 
|  div 
|  modulo 
|  equals 
|  not_equals 
|  case_equality 
|  case_inequality 
|  case_q 
|  not_case_q 
|  log_and 
|  log_or 
|  starstar 
|  lt 
|  le 
|  gt 
|  ge 
|  and 
|  or 
|  xor 
|  xnor 
|  xorn 
|  rshift 
|  lshift 
|  arshift 
|  alshift 
|  derive 
|  dderive ;

inc_or_dec_operator
locals []
:  increment 
|  decrement ;

increment
locals []
:  plus  plus ;

decrement
locals []
:  minus  minus ;

unary_module_path_operator
locals []
:  not 
|  compliment 
|  and 
|  nand 
|  or 
|  nor 
|  xor 
|  xnor 
|  xorn ;

binary_module_path_operator
locals []
:  equals 
|  not_equals 
|  log_and 
|  log_or 
|  and 
|  or 
|  xor 
|  xnor 
|  xorn ;

unbased_unsized_literal
locals []
:  scalar_constant0 
|  scalar_constant1 
|  escapequote  z_or_x ;

string_literal
locals []
:  string ;

attribute_instance
locals []
:  lparenstar  attr_spec (  comma  attr_spec  )*  starrparen ;

attr_spec
locals []
:  attr_name (  assign  constant_expression  )? ;

attr_name
locals []
:  identifier ;

array_identifier
locals []
:  identifier ;

block_identifier
locals []
:  identifier ;

bin_identifier
locals []
:  identifier ;

c_identifier
locals []
:  simple_identifier ;

cell_identifier
locals []
:  identifier ;

checker_identifier
locals []
:  identifier ;

class_identifier
locals []
:  identifier ;

class_variable_identifier
locals []
:  variable_identifier ;

clocking_identifier
locals []
:  identifier ;

config_identifier
locals []
:  identifier ;

const_identifier
locals []
:  identifier ;

constraint_identifier
locals []
:  identifier ;

covergroup_identifier
locals []
:  identifier ;

covergroup_variable_identifier
locals []
:  variable_identifier ;

cover_point_identifier
locals []
:  identifier ;

cross_identifier
locals []
:  identifier ;

dynamic_array_variable_identifier
locals []
:  variable_identifier ;

enum_identifier
locals []
:  identifier ;

escaped_identifier
locals []
:  esc_identifier ;

formal_identifier
locals []
:  identifier ;

function_identifier
locals []
:  identifier ;

generate_block_identifier
locals []
:  identifier ;

genvar_identifier
locals []
:  identifier ;

hierarchical_array_identifier
locals []
:  hierarchical_identifier ;

hierarchical_block_identifier
locals []
:  hierarchical_identifier ;

hierarchical_event_identifier
locals []
:  hierarchical_identifier ;

hierarchical_identifier
locals []
: (  dollarrootstr  dot  )? (  identifier  constant_bit_select  dot  )*  identifier ;

hierarchical_net_identifier
locals []
:  hierarchical_identifier ;

hierarchical_parameter_identifier
locals []
:  hierarchical_identifier ;

hierarchical_property_identifier
locals []
:  hierarchical_identifier ;

hierarchical_sequence_identifier
locals []
:  hierarchical_identifier ;

hierarchical_task_identifier
locals []
:  hierarchical_identifier ;

hierarchical_tf_identifier
locals []
:  hierarchical_identifier ;

hierarchical_variable_identifier
locals []
:  hierarchical_identifier ;

identifier
locals []
:  simple_identifier 
|  escaped_identifier ;

index_variable_identifier
locals []
:  identifier ;

interface_identifier
locals []
:  identifier ;

interface_instance_identifier
locals []
:  identifier ;

inout_port_identifier
locals []
:  identifier ;

input_port_identifier
locals []
:  identifier ;

instance_identifier
locals []
:  identifier ;

library_identifier
locals []
:  identifier ;

member_identifier
locals []
:  identifier ;

method_identifier
locals []
:  identifier ;

modport_identifier
locals []
:  identifier ;

module_identifier
locals []
:  identifier ;

net_identifier
locals []
:  identifier ;

output_port_identifier
locals []
:  identifier ;

package_identifier
locals []
:  identifier ;

package_scope
locals []
:  package_identifier  coloncolon 
|  dollarunitstr  coloncolon ;

parameter_identifier
locals []
:  identifier ;

port_identifier
locals []
:  identifier ;

production_identifier
locals []
:  identifier ;

program_identifier
locals []
:  identifier ;

property_identifier
locals []
:  identifier ;

ps_class_identifier
locals []
:  package_scope?  class_identifier ;

ps_covergroup_identifier
locals []
:  package_scope?  covergroup_identifier ;

ps_identifier
locals []
:  package_scope?  identifier ;

ps_or_hierarchical_array_identifier
locals []
:  ps_or_hierarchical_array_identifier_part1*  hierarchical_array_identifier ;

ps_or_hierarchical_array_identifier_part1
locals []
:  implicit_class_handle  dot 
|  class_scope 
|  package_scope ;

ps_or_hierarchical_net_identifier
locals []
:  package_scope?  net_identifier 
|  hierarchical_net_identifier ;

ps_or_hierarchical_property_identifier
locals []
:  package_scope?  property_identifier 
|  hierarchical_property_identifier ;

ps_or_hierarchical_sequence_identifier
locals []
:  package_scope?  sequence_identifier 
|  hierarchical_sequence_identifier ;

ps_or_hierarchical_tf_identifier
locals []
:  package_scope?  tf_identifier 
|  hierarchical_tf_identifier ;

ps_parameter_identifier
locals []
:  package_scope?  parameter_identifier 
|  ps_parameter_identifier_part1*  parameter_identifier ;

ps_parameter_identifier_part1
locals []
:  generate_block_identifier (  lbrack  constant_expression  rbrack  )?  dot ;

ps_type_identifier
locals []
: (  localcoloncolon  |  package_scope  )?  type_identifier ;

sequence_identifier
locals []
:  identifier ;

signal_identifier
locals []
:  identifier ;

simple_identifier
locals []
:  id ;

specparam_identifier
locals []
:  identifier ;

system_tf_identifier
locals []
:  tf_id ;

task_identifier
locals []
:  identifier ;

tf_identifier
locals []
:  identifier ;

terminal_identifier
locals []
:  identifier ;

topmodule_identifier
locals []
:  identifier ;

type_identifier
locals []
:  identifier ;

udp_identifier
locals []
:  identifier ;

bins_identifier
locals []
:  identifier ;

variable_identifier
locals []
:  identifier ;

number
locals []
:  minus?  edge_spec 
|  minus?  zero_or_one 
|  minus?  decimal_number 
|  octal_number 
|  binary_number 
|  hex_number 
|  real_number ;

eof
locals []
:  EOF ;

endmodulestr
locals []
:  ENDMODULESTR 
;

colon
locals []
:  COLON 
;

externstr
locals []
:  EXTERNSTR 
;

semi
locals []
:  SEMI 
;

modulestr
locals []
:  MODULESTR 
;

macromodulestr
locals []
:  MACROMODULESTR 
;

endinterfacestr
locals []
:  ENDINTERFACESTR 
;

interfacestr
locals []
:  INTERFACESTR 
;

lparen
locals []
:  LPAREN 
;

dotstar
locals []
:  DOTSTAR 
;

rparen
locals []
:  RPAREN 
;

endprogramstr
locals []
:  ENDPROGRAMSTR 
;

programstr
locals []
:  PROGRAMSTR 
;

checkerstr
locals []
:  CHECKERSTR 
;

endcheckerstr
locals []
:  ENDCHECKERSTR 
;

virtualstr
locals []
:  VIRTUALSTR 
;

classstr
locals []
:  CLASSSTR 
;

extendsstr
locals []
:  EXTENDSSTR 
;

endclassstr
locals []
:  ENDCLASSSTR 
;

packagestr
locals []
:  PACKAGESTR 
;

endpackagestr
locals []
:  ENDPACKAGESTR 
;

timeunit
locals []
:  TIMEUNIT 
;

div
locals []
:  DIV 
;

hash
locals []
:  HASH 
;

comma
locals []
:  COMMA 
;

typestr
locals []
:  TYPESTR 
;

dot
locals []
:  DOT 
;

lcurl
locals []
:  LCURL 
;

rcurl
locals []
:  RCURL 
;

inputstr
locals []
:  INPUTSTR 
;

outputstr
locals []
:  OUTPUTSTR 
;

inoutstr
locals []
:  INOUTSTR 
;

refstr
locals []
:  REFSTR 
;

assign
locals []
:  ASSIGN 
;

dollarfatalstr
locals []
:  DOLLARFATALSTR 
;

dollarerrorstr
locals []
:  DOLLARERRORSTR 
;

dollarwarningstr
locals []
:  DOLLARWARNINGSTR 
;

dollarinfostr
locals []
:  DOLLARINFOSTR 
;

defparamstr
locals []
:  DEFPARAMSTR 
;

bindstr
locals []
:  BINDSTR 
;

configstr
locals []
:  CONFIGSTR 
;

endconfigstr
locals []
:  ENDCONFIGSTR 
;

designstr
locals []
:  DESIGNSTR 
;

defaultstr
locals []
:  DEFAULTSTR 
;

instancestr
locals []
:  INSTANCESTR 
;

cellstr
locals []
:  CELLSTR 
;

libliststr
locals []
:  LIBLISTSTR 
;

usestr
locals []
:  USESTR 
;

clockingstr
locals []
:  CLOCKINGSTR 
;

disablestr
locals []
:  DISABLESTR 
;

iffstr
locals []
:  IFFSTR 
;

forkjoinstr
locals []
:  FORKJOINSTR 
;

alwaysstr
locals []
:  ALWAYSSTR 
;

conststr
locals []
:  CONSTSTR 
;

functionstr
locals []
:  FUNCTIONSTR 
;

newstr
locals []
:  NEWSTR 
;

staticstr
locals []
:  STATICSTR 
;

protectedstr
locals []
:  PROTECTEDSTR 
;

localstr
locals []
:  LOCALSTR 
;

randstr
locals []
:  RANDSTR 
;

randcstr
locals []
:  RANDCSTR 
;

purestr
locals []
:  PURESTR 
;

superstr
locals []
:  SUPERSTR 
;

endfunctionstr
locals []
:  ENDFUNCTIONSTR 
;

constraintstr
locals []
:  CONSTRAINTSTR 
;

solvestr
locals []
:  SOLVESTR 
;

beforestr
locals []
:  BEFORESTR 
;

derive
locals []
:  DERIVE 
;

ifstr
locals []
:  IFSTR 
;

elsestr
locals []
:  ELSESTR 
;

foreachstr
locals []
:  FOREACHSTR 
;

lbrack
locals []
:  LBRACK 
;

rbrack
locals []
:  RBRACK 
;

colonequals
locals []
:  COLONEQUALS 
;

colonslash
locals []
:  COLONSLASH 
;

localparamstr
locals []
:  LOCALPARAMSTR 
;

parameterstr
locals []
:  PARAMETERSTR 
;

specparamstr
locals []
:  SPECPARAMSTR 
;

varstr
locals []
:  VARSTR 
;

importstr
locals []
:  IMPORTSTR 
;

coloncolon
locals []
:  COLONCOLON 
;

star
locals []
:  STAR 
;

export
locals []
:  EXPORT 
;

startcoloncolonstar
locals []
:  STARTCOLONCOLONSTAR 
;

genvarstr
locals []
:  GENVARSTR 
;

vectoredstr
locals []
:  VECTOREDSTR 
;

scalaredstr
locals []
:  SCALAREDSTR 
;

typedefstr
locals []
:  TYPEDEFSTR 
;

enumstr
locals []
:  ENUMSTR 
;

structstr
locals []
:  STRUCTSTR 
;

unionstr
locals []
:  UNIONSTR 
;

automaticstr
locals []
:  AUTOMATICSTR 
;

stringstr
locals []
:  STRINGSTR 
;

packedstr
locals []
:  PACKEDSTR 
;

chandlestr
locals []
:  CHANDLESTR 
;

eventstr
locals []
:  EVENTSTR 
;

zero_or_one
locals []
:  Zero_Or_One 
;

edge_spec
locals []
:  EDGE_SPEC 
;

decimal_number
locals []
:  Decimal_number 
;

bytestr
locals []
:  BYTESTR 
;

shortintstr
locals []
:  SHORTINTSTR 
;

intstr
locals []
:  INTSTR 
;

longintstr
locals []
:  LONGINTSTR 
;

integerstr
locals []
:  INTEGERSTR 
;

timestr
locals []
:  TIMESTR 
;

bitstr
locals []
:  BITSTR 
;

logicstr
locals []
:  LOGICSTR 
;

regstr
locals []
:  REGSTR 
;

shortreal
locals []
:  SHORTREAL 
;

realstr
locals []
:  REALSTR 
;

realtimestr
locals []
:  REALTIMESTR 
;

supply0str
locals []
:  SUPPLY0STR 
;

supply1str
locals []
:  SUPPLY1STR 
;

tristr
locals []
:  TRISTR 
;

triandstr
locals []
:  TRIANDSTR 
;

triorstr
locals []
:  TRIORSTR 
;

triregstr
locals []
:  TRIREGSTR 
;

tri0str
locals []
:  TRI0STR 
;

tri1str
locals []
:  TRI1STR 
;

uwirestr
locals []
:  UWIRESTR 
;

wirestr
locals []
:  WIRESTR 
;

wandstr
locals []
:  WANDSTR 
;

worstr
locals []
:  WORSTR 
;

signedstr
locals []
:  SIGNEDSTR 
;

unsignedstr
locals []
:  UNSIGNEDSTR 
;

voidstr
locals []
:  VOIDSTR 
;

taggedstr
locals []
:  TAGGEDSTR 
;

highz1str
locals []
:  HIGHZ1STR 
;

highz0str
locals []
:  HIGHZ0STR 
;

strong0
locals []
:  STRONG0 
;

pull0str
locals []
:  PULL0STR 
;

weak0str
locals []
:  WEAK0STR 
;

strong1
locals []
:  STRONG1 
;

pull1str
locals []
:  PULL1STR 
;

weak1str
locals []
:  WEAK1STR 
;

smallstr
locals []
:  SMALLSTR 
;

mediumstr
locals []
:  MEDIUMSTR 
;

largestr
locals []
:  LARGESTR 
;

real_number
locals []
:  Real_number 
;

onestepstr
locals []
:  ONESTEPSTR 
;

pathpulsedollar
locals []
:  PATHPULSEDOLLAR 
;

dollar
locals []
:  DOLLAR 
;

taskstr
locals []
:  TASKSTR 
;

dpi_spec_ing2str
locals []
:  DPI_SPEC_ING2STR 
;

dpi_spec_ing1str
locals []
:  DPI_SPEC_ING1STR 
;

contextstr
locals []
:  CONTEXTSTR 
;

endtaskstr
locals []
:  ENDTASKSTR 
;

plus
locals []
:  PLUS 
;

minus
locals []
:  MINUS 
;

starstar
locals []
:  STARSTAR 
;

modulo
locals []
:  MODULO 
;

equals
locals []
:  EQUALS 
;

not_equals
locals []
:  NOT_EQUALS 
;

lt
locals []
:  LT 
;

le
locals []
:  LE 
;

gt
locals []
:  GT 
;

ge
locals []
:  GE 
;

modportstr
locals []
:  MODPORTSTR 
;

assertstr
locals []
:  ASSERTSTR 
;

propertystr
locals []
:  PROPERTYSTR 
;

assumestr
locals []
:  ASSUMESTR 
;

coverstr
locals []
:  COVERSTR 
;

expectstr
locals []
:  EXPECTSTR 
;

sequencestr
locals []
:  SEQUENCESTR 
;

restrictstr
locals []
:  RESTRICTSTR 
;

endpropertystr
locals []
:  ENDPROPERTYSTR 
;

casestr
locals []
:  CASESTR 
;

endcasestr
locals []
:  ENDCASESTR 
;

notstr
locals []
:  NOTSTR 
;

orstr
locals []
:  ORSTR 
;

andstr
locals []
:  ANDSTR 
;

orderive
locals []
:  ORDERIVE 
;

orimplies
locals []
:  ORIMPLIES 
;

endsequencestr
locals []
:  ENDSEQUENCESTR 
;

untypedstr
locals []
:  UNTYPEDSTR 
;

intersectstr
locals []
:  INTERSECTSTR 
;

first_matchstr
locals []
:  FIRST_MATCHSTR 
;

throughoutstr
locals []
:  THROUGHOUTSTR 
;

withinstr
locals []
:  WITHINSTR 
;

double_hash
locals []
:  DOUBLE_HASH 
;

diststr
locals []
:  DISTSTR 
;

letstr
locals []
:  LETSTR 
;

covergroupstr
locals []
:  COVERGROUPSTR 
;

endgroupstr
locals []
:  ENDGROUPSTR 
;

optiondot
locals []
:  OPTIONDOT 
;

type_optiondot
locals []
:  TYPE_OPTIONDOT 
;

withstr
locals []
:  WITHSTR 
;

samplestr
locals []
:  SAMPLESTR 
;

attheratelparen
locals []
:  ATTHERATELPAREN 
;

beginstr
locals []
:  BEGINSTR 
;

endstr
locals []
:  ENDSTR 
;

coverpointstr
locals []
:  COVERPOINTSTR 
;

wildcardstr
locals []
:  WILDCARDSTR 
;

binsstr
locals []
:  BINSSTR 
;

illegal_binsstr
locals []
:  ILLEGAL_BINSSTR 
;

ignore_binsstr
locals []
:  IGNORE_BINSSTR 
;

implies
locals []
:  IMPLIES 
;

crossstr
locals []
:  CROSSSTR 
;

not
locals []
:  NOT 
;

log_and
locals []
:  LOG_AND 
;

log_or
locals []
:  LOG_OR 
;

binsofstr
locals []
:  BINSOFSTR 
;

pulldownstr
locals []
:  PULLDOWNSTR 
;

pullupstr
locals []
:  PULLUPSTR 
;

cmosstr
locals []
:  CMOSSTR 
;

rcmosstr
locals []
:  RCMOSSTR 
;

bufif0str
locals []
:  BUFIF0STR 
;

bufif1str
locals []
:  BUFIF1STR 
;

notif0str
locals []
:  NOTIF0STR 
;

notif1str
locals []
:  NOTIF1STR 
;

nmosstr
locals []
:  NMOSSTR 
;

pmos
locals []
:  PMOS 
;

rnmosstr
locals []
:  RNMOSSTR 
;

rpmosstr
locals []
:  RPMOSSTR 
;

nandstr
locals []
:  NANDSTR 
;

norstr
locals []
:  NORSTR 
;

xorstrstr
locals []
:  XORSTRSTR 
;

xnorstr
locals []
:  XNORSTR 
;

bufstr
locals []
:  BUFSTR 
;

tranif0str
locals []
:  TRANIF0STR 
;

tranif1str
locals []
:  TRANIF1STR 
;

rtranif1str
locals []
:  RTRANIF1STR 
;

rtranif0str
locals []
:  RTRANIF0STR 
;

transtr
locals []
:  TRANSTR 
;

rtranstr
locals []
:  RTRANSTR 
;

generatestr
locals []
:  GENERATESTR 
;

endgeneratestr
locals []
:  ENDGENERATESTR 
;

forstr
locals []
:  FORSTR 
;

primitivestr
locals []
:  PRIMITIVESTR 
;

endprimitivestr
locals []
:  ENDPRIMITIVESTR 
;

tablestr
locals []
:  TABLESTR 
;

endtablestr
locals []
:  ENDTABLESTR 
;

initialstr
locals []
:  INITIALSTR 
;

binary_number
locals []
:  Binary_number 
;

questinmark
locals []
:  QUESTINMARK 
;

id
locals []
:  ID 
;

assignstrstr
locals []
:  ASSIGNSTRSTR 
;

aliasstr
locals []
:  ALIASSTR 
;

always_combstr
locals []
:  ALWAYS_COMBSTR 
;

always_latchstr
locals []
:  ALWAYS_LATCHSTR 
;

always_ffstr
locals []
:  ALWAYS_FFSTR 
;

finalstr
locals []
:  FINALSTR 
;

plusequals
locals []
:  PLUSEQUALS 
;

minusequals
locals []
:  MINUSEQUALS 
;

startequals
locals []
:  STARTEQUALS 
;

slashequals
locals []
:  SLASHEQUALS 
;

percentileequal
locals []
:  PERCENTILEEQUAL 
;

andequals
locals []
:  ANDEQUALS 
;

orequal
locals []
:  OREQUAL 
;

xorequal
locals []
:  XOREQUAL 
;

lshift_assign
locals []
:  LSHIFT_ASSIGN 
;

rshift_assign
locals []
:  RSHIFT_ASSIGN 
;

unsigned_lshift_assign
locals []
:  UNSIGNED_LSHIFT_ASSIGN 
;

unsigned_rshift_assign
locals []
:  UNSIGNED_RSHIFT_ASSIGN 
;

deassignstr
locals []
:  DEASSIGNSTR 
;

forcestr
locals []
:  FORCESTR 
;

releasestr
locals []
:  RELEASESTR 
;

forkstr
locals []
:  FORKSTR 
;

joinstr
locals []
:  JOINSTR 
;

join_anystr
locals []
:  JOIN_ANYSTR 
;

join_namestr
locals []
:  JOIN_NAMESTR 
;

repeatstr
locals []
:  REPEATSTR 
;

attherate
locals []
:  ATTHERATE 
;

attheratestar
locals []
:  ATTHERATESTAR 
;

lparenstarrparen
locals []
:  LPARENSTARRPAREN 
;

returnstr
locals []
:  RETURNSTR 
;

breakstr
locals []
:  BREAKSTR 
;

continuestr
locals []
:  CONTINUESTR 
;

waitstr
locals []
:  WAITSTR 
;

wait_orderstr
locals []
:  WAIT_ORDERSTR 
;

derivegt
locals []
:  DERIVEGT 
;

uniquestr
locals []
:  UNIQUESTR 
;

unique0str
locals []
:  UNIQUE0STR 
;

prioritystr
locals []
:  PRIORITYSTR 
;

matchesstr
locals []
:  MATCHESSTR 
;

insidestr
locals []
:  INSIDESTR 
;

casezstr
locals []
:  CASEZSTR 
;

casexstr
locals []
:  CASEXSTR 
;

andandand
locals []
:  ANDANDAND 
;

randcasestr
locals []
:  RANDCASESTR 
;

escapelcurl
locals []
:  ESCAPELCURL 
;

foreverstr
locals []
:  FOREVERSTR 
;

whilestr
locals []
:  WHILESTR 
;

dostr
locals []
:  DOSTR 
;

escapequote
locals []
:  ESCAPEQUOTE 
;

hash_zero
locals []
:  HASH_ZERO 
;

endclockingstr
locals []
:  ENDCLOCKINGSTR 
;

globalstr
locals []
:  GLOBALSTR 
;

randsequencestr
locals []
:  RANDSEQUENCESTR 
;

or
locals []
:  OR 
;

specifystr
locals []
:  SPECIFYSTR 
;

endspecifystr
locals []
:  ENDSPECIFYSTR 
;

pulsestyle_oneventstr
locals []
:  PULSESTYLE_ONEVENTSTR 
;

pulsestyle_ondetectstr
locals []
:  PULSESTYLE_ONDETECTSTR 
;

showcancelledstr
locals []
:  SHOWCANCELLEDSTR 
;

noshowcancelledstr
locals []
:  NOSHOWCANCELLEDSTR 
;

stargt
locals []
:  STARGT 
;

posedgestr
locals []
:  POSEDGESTR 
;

negedgestr
locals []
:  NEGEDGESTR 
;

edgestr
locals []
:  EDGESTR 
;

ifnonestr
locals []
:  IFNONESTR 
;

dollarsetupstr
locals []
:  DOLLARSETUPSTR 
;

dollarholdstr
locals []
:  DOLLARHOLDSTR 
;

dollarsetupholdstr
locals []
:  DOLLARSETUPHOLDSTR 
;

dollarrecoverystr
locals []
:  DOLLARRECOVERYSTR 
;

dollarremovalstr
locals []
:  DOLLARREMOVALSTR 
;

dollarrecremstr
locals []
:  DOLLARRECREMSTR 
;

dollarskewstr
locals []
:  DOLLARSKEWSTR 
;

dollartimeskewstr
locals []
:  DOLLARTIMESKEWSTR 
;

dollarfullskewstr
locals []
:  DOLLARFULLSKEWSTR 
;

dollarperiodstr
locals []
:  DOLLARPERIODSTR 
;

dollaewidthstr
locals []
:  DOLLAEWIDTHSTR 
;

dollarnochangestr
locals []
:  DOLLARNOCHANGESTR 
;

z_or_x
locals []
:  Z_or_X 
;

compliment
locals []
:  COMPLIMENT 
;

case_equality
locals []
:  CASE_EQUALITY 
;

case_inequality
locals []
:  CASE_INEQUALITY 
;

rshift
locals []
:  RSHIFT 
;

lshift
locals []
:  LSHIFT 
;

pluscolon
locals []
:  PLUSCOLON 
;

minuscolon
locals []
:  MINUSCOLON 
;

stdcoloncolon
locals []
:  STDCOLONCOLON 
;

randomizestr
locals []
:  RANDOMIZESTR 
;

nullstr
locals []
:  NULLSTR 
;

alshift
locals []
:  ALSHIFT 
;

arshift
locals []
:  ARSHIFT 
;

case_q
locals []
:  CASE_Q 
;

not_case_q
locals []
:  NOT_CASE_Q 
;

and
locals []
:  AND 
;

xor
locals []
:  XOR 
;

xnor
locals []
:  XNOR 
;

xorn
locals []
:  XORN 
;

thisstr
locals []
:  THISSTR 
;

localcoloncolon
locals []
:  LOCALCOLONCOLON 
;

time_unit
locals []
:  TIME_UNIT 
;

nand
locals []
:  NAND 
;

nor
locals []
:  NOR 
;

dderive
locals []
:  DDERIVE 
;

scalar_constant0
locals []
:  SCALAR_CONSTANT0 
;

scalar_constant1
locals []
:  SCALAR_CONSTANT1 
;

string
locals []
:  STRING 
;

lparenstar
locals []
:  LPARENSTAR 
;

starrparen
locals []
:  STARRPAREN 
;

esc_identifier
locals []
:  ESCAPED_IDENTIFIER 
;

dollarrootstr
locals []
:  DOLLARROOTSTR 
;

dollarunitstr
locals []
:  DOLLARUNITSTR 
;

tf_id
locals []
:  TF_ID 
;

octal_number
locals []
:  Octal_number 
;

hex_number
locals []
:  Hex_number 
;


pragma_def 
locals []
: PRAGMA_SPECIFIER PRAGMA_ID pragma_text NEW_LINE;

pragma_text 
locals []
: (PRAGMA_ID | MODE_TEXT ) *;
