//////////////////////////////////////////////////////////////////////
////                                                              ////
//// Copyright (C) 2014 leishangwen@163.com                       ////
////                                                              ////
//// This source file may be used and distributed without         ////
//// restriction provided that this copyright statement is not    ////
//// removed from the file and that any derivative work contains  ////
//// the original copyright notice and the associated disclaimer. ////
////                                                              ////
//// This source file is free software; you can redistribute it   ////
//// and/or modify it under the terms of the GNU Lesser General   ////
//// Public License as published by the Free Software Foundation; ////
//// either version 2.1 of the License, or (at your option) any   ////
//// later version.                                               ////
////                                                              ////
//// This source is distributed in the hope that it will be       ////
//// useful, but WITHOUT ANY WARRANTY; without even the implied   ////
//// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      ////
//// PURPOSE.  See the GNU Lesser General Public License for more ////
//// details.                                                     ////
////                                                              ////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// Module:  id_ex
// File:    id_ex.v
// Author:  Lei Silei
// E-mail:  leishangwen@163.com
// Description: ID/EX階段的暫存器
// Revision: 1.0
//////////////////////////////////////////////////////////////////////

`include "defines.v"

module id_ex(
	input wire clk,
	input wire rst,
	
	//從解碼階段傳遞的資訊
	input wire[`AluOpBus]         id_aluop_i,
	input wire[`AluSelBus]        id_alusel_i,
	input wire[`RegBus]           id_reg1_i,
	input wire[`RegBus]           id_reg2_i,
	input wire[`RegAddrBus]       id_wd_i,
	input wire                    id_wreg_i,	
	
	//傳遞到執行階段的資訊
	output reg[`AluOpBus]         ex_aluop_o,
	output reg[`AluSelBus]        ex_alusel_o,
	output reg[`RegBus]           ex_reg1_o,
	output reg[`RegBus]           ex_reg2_o,
	output reg[`RegAddrBus]       ex_wd_o,
	output reg                    ex_wreg_o
);

	always @ (posedge clk) begin
		if (rst == `RstEnable) begin
			ex_aluop_o <= `EXE_NOP_OP;
			ex_alusel_o <= `EXE_RES_NOP;
			ex_reg1_o <= `ZeroWord;
			ex_reg2_o <= `ZeroWord;
			ex_wd_o <= `NOPRegAddr;
			ex_wreg_o <= `WriteDisable;
		end else begin		
			ex_aluop_o <= id_aluop_i;
			ex_alusel_o <= id_alusel_i;
			ex_reg1_o <= id_reg1_i;
			ex_reg2_o <= id_reg2_i;
			ex_wd_o <= id_wd_i;
			ex_wreg_o <= id_wreg_i;		
		end
	end
	
endmodule
