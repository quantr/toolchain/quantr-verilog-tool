
import hk.quantr.quantrverilogtool.antlr.Sv2012Lexer;
import hk.quantr.quantrverilogtool.antlr.Sv2012Parser;
import hk.quantr.quantrverilogtool.antlr.Sv2012Parser.Library_textContext;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Test3 {

	@Test
	public void test() throws Exception {
		Sv2012Lexer lexer = new Sv2012Lexer(CharStreams.fromString(IOUtils.toString(Test1.class.getResource("/pc_reg.v"), "utf-8")));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);

		Sv2012Parser parser = new Sv2012Parser(tokenStream);
		Library_textContext context = parser.library_text();
		System.out.println(context);
	}
}
