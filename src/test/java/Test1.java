
import hk.quantr.quantrverilogtool.antlr.Verilog2001Lexer;
import hk.quantr.quantrverilogtool.antlr.Verilog2001Parser;
import hk.quantr.quantrverilogtool.antlr.Verilog2001Parser.Source_textContext;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Test1 {

	@Test
	public void test() throws Exception {
		Verilog2001Lexer lexer = new Verilog2001Lexer(CharStreams.fromString(IOUtils.toString(Test1.class.getResource("/regfile.v"), "utf-8")));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);

		Verilog2001Parser parser = new Verilog2001Parser(tokenStream);
		Source_textContext context = parser.source_text();
		System.out.println();
	}
}
