
import hk.quantr.quantrverilogtool.antlr.VerilogPrimeLexer;
import hk.quantr.quantrverilogtool.antlr.VerilogPrimeParser;
import hk.quantr.quantrverilogtool.antlr.VerilogPrimeParser.Source_textContext;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Test2 {

	@Test
	public void test() throws Exception {
		VerilogPrimeLexer lexer = new VerilogPrimeLexer(CharStreams.fromString(IOUtils.toString(Test1.class.getResource("/pc_reg.v"), "utf-8")));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);

		VerilogPrimeParser parser = new VerilogPrimeParser(tokenStream);
		Source_textContext context = parser.source_text();
		System.out.println(context);
	}
}
